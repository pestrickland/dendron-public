---
id: 7h8s3e14zxy9p1q1qsfd2l0
title: Team Self Assessment
desc: ''
updated: 1684093658954
created: 1684093658954
---

## Team Self-Assessment

This self-assessment activity could be carried out in-person or online and could help to have a loosely-structured approach to understanding the perception the team has of itself:

<https://www.sessionlab.com/methods/team-self-assessment>
