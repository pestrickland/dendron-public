---
id: kfq3nlh8pnvgzzkvrwqf5iu
title: Umbrella Managers
desc: >-
  Well-intentioned leaders who want to protect their teams from inclement
  organisational weather
updated: 1680960211737
created: 1680959099564
---

- The umbrella manager feels personally responsible for every decision[^1]. They can't stay on top of all the details and become overwhelmed. The team becomes passive and complacent; the manager becomes the bottleneck
- Team members don't benefit from learning to navigate the storm themselves. Employees miss out from forming strong cross-functional relationships and therefore from greater organisational visibility
- The organisation experiences reduced productivity and innovation from the team. To be nimble and adapt to changing circumstances is almost impossible; the team rigidly holds onto outdated plans, wasting time and resources

To solve this:

- Let go of the need to protect employees
- Hand out raincoats rather than get out the umbrella; provide the tools for self-sufficiency rather than all the answers
- Face your fears directly - why are you like this?
- Assume your employees can solve the problem; become a trusted partner rather than a solution provider; ask them what options _they have_ rather than offering your own solution
- Embrace short-term stumbles for long-term gains; they provide learning experiences, uncover weaknesses, areas for improvement and opportunities
- Lean on your leadership strengths; stop focusing on every detail and understand which decisions are critical; shape what good looks like and align this with the wider organisation

To "empower" the team:

- Provide tools and support for navigating challenges
- Help the team embrace discomfort and maintain perspective
- Be curious rather than afraid of uncertain situations
- Communicate assumption, needs, expectations to reduce guesswork
- Nurture a "safe to try" environment

When things _do_ go wrong:

- Be optimistic
- Focus on solution-oriented behaviour
- Do not blame

  [^1]: <https://hbr.org/2023/03/how-to-equip-your-team-to-problem-solve-without-you>
