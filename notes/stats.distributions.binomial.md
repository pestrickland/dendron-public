---
id: i8wheprb14wikqnxpz3wynq
title: Binomial Distribution
desc: ""
updated: 1732802618269
created: 1721481074735
---

This is the generalisation when there are _N_ independent Bernoullis.

$$
X \sim \mathrm{Bin}(n,p)
$$

$$
f(X=x\vert p)= {n \choose x} p^x(1-p)^{n-x}
$$

Where ${n \choose x}$ is the combinatoric term:

$$
{n \choose x} = \frac{n!}{x!(n-x)!} \quad \text{for } x \in \{0,1...n\}
$$

### Expected Value

$$
E [X] = np
$$

### Variance

$$
\mathrm{Var}(X) = np(1-p)
$$
