---
id: kuj680z1i3v0iyqk9hgirqe
title: Independence
desc: ""
updated: 1725830922592
created: 1725830922592
---

If events $A$ and $B$ are independent,

$$
P ( A \vert B) = P(A)
$$

This also means that:

$$
P ( A \cap B ) = P(A) P(B)
$$
