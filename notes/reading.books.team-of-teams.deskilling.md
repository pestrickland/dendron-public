---
id: 55keos2itxk9uovg2mrrjsj
title: Deskilling
desc: ""
updated: 1711903330130
created: 1708798952761
---

Noted in _[[reading.books.team-of-teams]]_ Chapter 2: Clockwork

> ...being recast as mindless cogs in a larger machine was degrading

Making [[red workers|reading.books.leadership-is-language.red-work]] out of [[blue workers|reading.books.leadership-is-language.blue-work]]; deskilling.

> We have other men paid for thinking

An insult to workers from Taylor:

> [A labourer] shall be so stupid and so phlegmatic that he more nearly resembles in his mental make-up the ox than any other type... the workman who is best suited to handling pig iron is unable to understand the real science of doing this class of work. He is so stupid that the word "percentage" has no meaning to him, and he must consequently be trained by a man more intelligent than himself into the habit of working in accordance with the laws of this science before he can be successful.
