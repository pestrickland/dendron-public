---
id: ffwglar14wootmadubai9n1
title: Skills Gap Analysis
desc: ''
updated: 1689439383500
created: 1689437351257
---

<https://peoplemanagingpeople.com/strategy-operations/skills-gap-analysis>

## What

- Understand workforce potential and gaps that could hinder business longevity
- Conduct every 3 to 6 months

## Why

- Every 5 years, an employee's abilities and knowledge are about half as valuable as they were before?![^1]
- It allows better awareness of workforce's skill sets
- It enhances employees' learning and development
- It reinforces strategic workforce planning
- It can help you be more intentional when recruiting
- It helps foster high-performing teams
- It helps develop future-proof learning and development plans
- It helps you gain a competitive advantage

## How

### Decide Plan and Scope

- Individual, based on job role requirements vs worker skills
- Team, looking at necessary capabilities or knowledge for a given project vs external recruitment

### Identify Critical Skills

- What skills are valuable currently?
- What abilities do workers need to continue to perform
- Look at macro-skills (critical thinking, problem-solving, leadership)
- Look at micro-skills (technical, academic skills)

### Assess Skills to Identify Gaps

### Act on the Data to Close the Gaps

[^1]: <https://www.weforum.org/agenda/2017/07/skill-reskill-prepare-for-future-of-work/>
