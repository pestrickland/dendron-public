---
id: z9zn6vtnq22kc2qufh7jy7x
title: Active Listening
desc: ''
updated: 1690025100688
created: 1690024380638
tags:
  - leadership
  - facilitation
  - listening
  - productivity
---

<https://productcoalition.com/become-a-better-facilitator-with-5-tips-for-improving-your-active-listening-skills-a1b93e98d4c7>

Don't:

- Just think ahead about what you're going to say or how you're going to reply
- Interject or jump in to respond, especially if you can't play back what you just heard
- React before filtering your thoughts, checking yourself for personal bias

To alleviate these natural traits, building active listening skills is similar to attaining muscle memory.

One of the key roles of facilitating a discussion is being able to:

- Receive the information being shared
- Synthesise the information
- Play it back
- Paraphrase or sum up the information so that you could share a concise summary with others

## Five Tips

Move from a performance mindset (trying to have all the answers, being the box) to a curiosity mindset. Listen and ask questions to help foster group discussion and exploration. Democratise the sharing of information among participants.

Ask clarifying questions and play back what you heard. The replaying should help people to realise if their message has been misinterpreted, allowing them to correct the record.

Use non-verbal methods such as holding eye contact, maintaining open body language and not checking your phone. Be present and stay focused on the people you are working with.

Don't judge. Don't jump to conclusions and make assumptions about others. You'll be wrong. You may have an aversion to someone that may be less to do with them or their ideas, and more to do with some personal baggage or biases you have.

Embrace empathy by acknowledging contributions, validating ideas and [[thanking people|resource.tools.thank-people]] for sharing.
