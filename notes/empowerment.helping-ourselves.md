---
id: jhvf35u2yrg8sb3s6hy7z9p
title: Helping Ourselves
desc: ""
updated: 1708879688640
created: 1708878459073
---

Noted in _[[reading.books.team-of-teams]]_ Chapter 2: Clockwork

Do we fail to help ourselves because we assume it is not (any longer) our job?

> [W]e still think of organisational leaders as planners, synchronisers, and coordinators
