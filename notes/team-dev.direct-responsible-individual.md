---
id: ir9x7v3ouilajq5n4pdzxfa
title: Direct Responsible Individual
desc: ''
updated: 1684093684939
created: 1684093645759
---

## Direct Responsible Individual

- Gives a single person responsibility for something
- They don't do everything themselves but organises the team, makes key decisions, owns the timeline and results
- Leans towards "owners" of EWAs but the key element is to be more of a leader of the output of the EWA rather than the sole person doing everything

[link to Forbes article about this](https://www.forbes.com/sites/quora/2012/10/02/how-well-does-apples-directly-responsible-individual-dri-model-work-in-practice/?sh=2234c3f6194c) sourced from [Doist blog post](https://async.twist.com/asynchronous-communication/) tools

![[productivity.silos]]
