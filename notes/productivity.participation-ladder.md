---
id: 684z61s1s06hfddj039eb90
title: Arnstein's Ladder of Citizen Participation
desc: ''
updated: 1683897790594
created: 1683897610191
---
 
<https://www.citizenshandbook.org/arnsteinsladder.html>

![](/assets/images/2023-05-12-14-03-11.png)

The ladder is a guide to seeing who has power when important decisions are being made. It has survived for so long because people continue to confront processes that refuse to consider anything beyond the bottom rungs.

Here is how David Wilcox describes the 8 rungs of the ladder at www.partnerships.org.uk/part/arn.htm:

1. **Manipulation** and 2 **Therapy**. Both are non participative. The aim is to cure or educate the participants. The proposed plan is best and the job of participation is to achieve public support through public relations.
2. **Informing**. A most important first step to legitimate participation. But too frequently the emphasis is on a one way flow of information. No channel for feedback.
3. **Consultation**. Again a legitimate step attitude surveys, neighbourhood meetings and public enquiries. But Arnstein still feels this is just a window dressing ritual.
4. **Placation**. For example, co-option of hand-picked ‘worthies’ onto committees. It allows citizens to advise or plan ad infinitum but retains for power holders the right to judge the legitimacy or feasibility of the advice.
5. **Partnership**. Power is in fact redistributed through negotiation between citizens and power holders. Planning and decision-making responsibilities are shared e.g. through joint committees.
6. **Delegation**. Citizens holding a clear majority of seats on committees with delegated powers to make decisions. Public now has the power to assure accountability of the programme to them.
7. **Citizen Control**. Have-nots handle the entire job of planning, policy making and managing a programme e.g. neighbourhood corporation with no intermediaries between it and the source of funds.

