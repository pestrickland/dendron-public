---
id: p445af0fmdzpxgw1jbn0akd
title: Gamma Distribution
desc: ""
updated: 1732802590822
created: 1731530413291
---

## Mean

$$
\frac{\alpha}{\beta}
$$

## Variance

$$
\frac{\alpha}{\beta^2}
$$
