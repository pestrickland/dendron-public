---
id: xk5a538zqwwni2muj6btuha
title: The Team Onion
desc: ""
updated: 1700306285446
created: 1700300245817
tags:
  - book
  - team-onion
---

## Citation Info

<!-- Comment: you can replace this biblatex section with a citation template in whatever style you tend to use, if you don't use Pandoc or something similar for generating citations. I currently edit these citations by hand. -->

```biblatex
@book{${1:link},
  author    = {Emily Webber},
  title     = {The Team Onion},
  year      = {},
  volume    = {},
  edition   = {},
  series    = {},
  number    = {},
  publisher = {},
  address   = {},
  pages     = {},
  note      = {},
  origdate  = {},
  isbn      = {}
}
```

<!-- Comment: You can change any of the below text. Make it your own! -->

## What was the thesis? Main argument?

## Were there any side arguments that were significant?

## How does this text fit within the literature? Historiography?

(What is it adding to the arguments already out there? does it challenge dominant trends in the literature?)

## What methodology did the author use?

## Evidence? What kinds of sources did the author use?

## Organisation? How did the author organise the flow of the argument and narrative?

## Critiques? Do you see any problems? Praise?

## Epistemology? (this is less important based on your field)

## Anything else of interest you would want to remember? Maybe something that will be useful for your dissertation.
