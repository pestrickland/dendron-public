---
id: c5w5wl86fdsrkqe7qkg7p79
title: Futurespective
desc: ''
updated: 1705757681475
created: 1705757177607
---

See this:

<https://neil-vass.com/futurespectives-learning-from-failures-that-havent-happened-yet>

AKA "[[pre-mortem|team-dev.pre-mortem]]".

## Simulating the Future

- Consider a possible future
- Discuss how likely that future scenario seems
- Agree what you’d like to do about it (ideas to make good futures more likely to happen, or ways to avoid bad ones)

## How to Run it

- Read out a series of prompts about an imagined future for this piece of work, and the team fills in the blanks by writing whatever comes to mind
- Keep notes in order (can write responses on post-its, index cards, or whatever virtual equivalent you like)
- Don’t discuss things as they happen – there might be chuckles, groans or other hints on how people are feeling, but we're looking for quiet reflection and writing for this part
- At the end we’ll collect and compare answers, to see what we all came up with – and agree what we want to do with this

## Prompts

Rather than construct an entire fictional scenario yourself, allow the team to develop it:

"We've just finished this piece of work, and it was a disaster. I’m still too upset to talk about it. Could you just write about it for me?

- What was the end result? How bad was it?
- And what happened to cause that?
- But that wasn’t the only thing that went wrong. What else was a huge problem?
- And what did we try right away to fix that?
- It didn’t work. How did our panic fix only make things worse?
- What was a problem from early on, that we tried to ignore?
- What always goes wrong – and hit us here too?
- Someone left – got a promotion, took a sabbatical, or something – and it really affected us. Who was it?
- And what did we lose? What skills/knowledge/support?
- And what huge problem did that lead to?
- Who was expecting something this project was just never going to give them?
- And what was it they were expecting?
- What did we assume would be fine, and it really really wasn’t?
- And on top of all that, what else went wrong?
