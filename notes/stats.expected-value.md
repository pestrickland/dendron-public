---
id: ikb1wqk0rbex6yttmygad14
title: Expected Values
desc: ''
updated: 1722786036875
created: 1721479969008
---

Also known as the _expectation_ or _mean_, the expected value of a random variable $X$ is denoted as $E(X)$. Is there any need to think about it as unique to what we normally think about as the mean/average?

The expectation _can_ be thought of as the average value, e.g. if we took samples of a random variable $X$, the average of the samples would be close to $E(X)$.

If $X$ is discrete-valued:

$$
E(X) = \sum_x x \cdot P(X=x) =\sum_x x \cdot f(x)
$$

For a continuous random variable, with a probability density function $f(x)$, then:

$$
E(X) = \int_{-\infty}^{\infty} x \cdot f(x) \mathrm{d}x
$$

## Multiple Variables

First, let:

$$
E(X) = \mu x \\
E(Y) = \mu y
$$

And now:

$$
Z = aX + bY + c
$$

Then,

$$
E(Z) = E(aX + bY +c) \\
E(Z) = aE(X) + bE(Y) + c \\
E(Z) = a\mu x + b\mu y + c
$$

We can also do the same for functions of $X$:

$$
g(X) = 2 / X \\
E(g(X)) = \int_{-\infty}^{\infty} g(x)f(x)\mathrm{d}x \\
E(g(X)) = \int_{-\infty}^{\infty} {2 \over x}f(x) \mathrm{d}x
$$

Note that, in general $E(g(X)) \neq g(E(X))$.

### Example

Let $X$ have a PDF $f(x) = 3x^2 𝟙_{\{0 \leq x \leq 1\}}(x)$. Find $E(X)$ and $E(X^2)$.

$$
\begin{align}
E(X) &= \int_{-\infty}^{\infty} x \cdot 3x^2 𝟙_{\{0 \leq x \leq 1\}}(x) \mathrm{d}x \\
&= \int_0^1 x \cdot 3x^2 \mathrm{d}x = \int_0^1 3x^3 \\
&= {3 \over 4}\left. x^4 \right \rvert^{x=1}_{x=0} \\
&= {3 \over 4}(1-0) \\
&= {3 \over 4}
\end{align}
$$

Now:

$$
\begin{align}
E(X^2) &= \int_{- \infty}^{\infty} x^2 \cdot 3x^2 𝟙_{\{0 \leq x \leq 1\}}(x) \mathrm{d}x \\
&= \int_0^1 x^2 \cdot 3x^2 \mathrm{d}x = \int_0^1 3x^4 \mathrm{d}x \\
&= {3 \over 5}  \left. x^5 \right\rvert_{x=0}^{x=1} \\
&= {3 \over 5}(1-0) \\
&= {3 \over 5}
\end{align}
$$
