---
id: 49771a0m3ja90og3pxq5jag
title: Micromanagers
desc: ""
updated: 1714914615390
created: 1673701309525
tags:
  - research
  - leadership
  - management
  - teams
---

Don't be a micromanager. No one wants one, no one wants to be one, and they don't usually consider themselves to be them. [^1]

## The Signs

- You don't trust your team (or a particular team member) to reach their goals
- You get itchy to check progress and results
- You think it saves time if you "just do it yourself"
- You focus on (dictating) the "how" instead of the "what" and "why"
- You do not prioritise more strategic tasks (where you can add actual value)
- You are not clear about your expectations when you delegate tasks
- You are too detail orientated, especially when details compete with accomplishments
- You do not have an open mind for different or alternative approaches to reach goals
- You have a constant fear of control
- You, and quite possibly some other team members, feel close to being burned out

Fortunately, although I recognise a lot of these traits, I don't think I am actively enacting many of them.

## Ask Yourself

"What would happen if teams could choose their own leaders? Would you still be chosen to lead your own team?"

[^1]: <https://corporate-rebels.com/fire-micromanagers>
