---
id: 92n9lqc3xn17i027akvdd0v
title: Conjugate Distribution
desc: ""
updated: 1732802654197
created: 1732802439727
---

What is a conjugate distribution?

- For a [[stats.distributions.beta]] distribution, the [[stats.distributions.gamma]] is its conjugate
- For an [[stats.distributions.exponential]], the [[stats.distributions.gamma]] is its conjugate
- For a [[stats.distributions.normal]], the [[stats.distributions.normal]] is its conjugate
