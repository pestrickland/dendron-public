---
id: ld2frxtomszlhip65yclbpd
title: Empowered Execution
desc: ''
updated: 1685285578976
created: 1685285517380
---

According to the McChrystal model[^1]:

> Teams must push decision authorities down as far as possible, enabling people closest to the challenge to take rapid action within the bounds of acceptable risk.

[^1]: Livingston, David, _What Kind of Leader Can Lead a Team of Teams? The 6 Principles of Leading Like a Gardene_, McChrystal Group, <https://www.mcchrystalgroup.com/insights/what-kind-of-leader-can-lead-a-team-of-teams-the-6-principles-of-leading-like-a-gardener/>, 12 October 2022.
