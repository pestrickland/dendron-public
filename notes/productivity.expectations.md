---
id: 8dluvac0unhfyb1vgkzagoq
title: Expectations
desc: ""
updated: 1696432074252
created: 1696104681227
---

"When you're in the middle of the work, set your expectations high. It's unlikely your performance will exceed the standard you set for yourself. High expectations encourage you to keep reaching and fulfil your potential.

Once the work is done, release yourself from your expectations. The fastest way to ruin a good outcome is to tell yourself it's not good enough. Your expectations dictate your happiness more than your results.

Expectations can be helpful as a motivator and unhelpful as a measuring stick. Now that the work is done you can rest easy knowing you tried your best. You've already won."
