---
id: 27vuv1ir3q4xvwhux09jrgy
title: Office Vs Remote
desc: ""
updated: 1699905182483
created: 1695124461935
tags:
  - micropractice
---

1. Ask your team members to look at the tasks that they have for the upcoming week and group them into:

   - Remote tasks: video calls or deep work
   - Office tasks: face-to-face interaction, informal meetings,

2. Create a simple decision matrix that outlines when it's best to work remotely and when it's best to go to the office based on the types of tasks
3. Establish rules:

   - If 70% of the tasks require focused time or will be spent on video calls, stay at home
   - If the tasks involve meeting collaborating and benefit from physical presence, go to the office

4. Have each team member reflect on their own tasks and encourage them to identify any adjustments they should make in their work habits
5. Give them the flexibility to adjust their individual and team ways of working

A Cultural Micropractice taken from <https://www.human.pm/>
