---
id: e2xmcvr8sc5rdpzjzjrzar7
title: Card Kits
desc: ''
updated: 1687174011203
created: 1685204002737
---

<https://methodkit.com/>

I like the idea of being able to come up with a set of cards that focus on the principles that we'd like to develop (and to allow people to recognise when those behaviours that we do not like are present).

I'm making a list [[here|resource.cards]].
