---
id: dp3cfbl8jwu28ig0orraw24
title: Weeknotes
desc: ""
updated: 1710624622704
created: 1696454807533
---

- <https://gilest.org/weeknotes-tips.html>
- <https://gilest.org/weeknotes-rules.html>
- <https://doingweeknotes.com/>

Also by [[Giles|people.giles-turnbull]], see [[productivity.presentations]].
