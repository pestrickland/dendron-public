---
id: i0f5ib7807r07jnri0cluga
title: Hierarchy of Controls
desc: ''
updated: 1670239388213
created: 1670239230460
---

> Controlling exposures to hazards in the workplace is vital to protecting workers. The hierarchy of controls is a way of determining which actions will best control exposures. The hierarchy of controls has five levels of actions to reduce or remove hazards. The preferred order of action based on general effectiveness is:
>
> 1. Elimination
> 2. Substitution
> 3. Engineering controls
> 4. Administrative controls
> 5. Personal protective equipment (PPE)
>
> Using this hierarchy can lower worker exposures and reduce risk of illness or injury.

_Source: <https://www.cdc.gov/niosh/topics/hierarchy/default.html>_
