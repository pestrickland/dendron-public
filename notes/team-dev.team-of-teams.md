---
id: mpdo5xzqb18mowdc4vxq63h
title: Team of Teams
desc: ''
updated: 1685287231260
created: 1685284519324
tags:
  - leadership
  - productivity
---

There are four tenets of the model[^1]:

- [[productivity.trust]]
- Common [[Purpose|productivity.purpose]]
- [[Shared Consciousness|productivity.shared-consciousness]]
- [[Empowered Execution|empowerment.empowered-execution]]

## Leading Like a Gardener

- Break up hard soil
- Plant seeds in the right place
- Ensure essential nutrients are available
- Remove barriers that block light
- Set up a regular cadence to water and weed
- Give the plant time and space to grow

Some choice words from the same model[^1]:

> - Gardeners routinely evaluate current operations and proactively make changes as needed
> - Mission first, people always
> - Gardeners take time to understand their team members' unique skillsets and match those capabilities to the requirements of a particular situation
> - Gardeners ruthlessly prioritize essential tasks and then ensure they are resourced appropriately
> - Holding periodic “All-Hands” conference calls or leaning on the executive leadership team to span silos will not solve this challenge [of all team members needing to have the full picture to ensure the right decisions are made]
> - Gardeners set up defined processes and effective technological systems to reinforce priorities and share relevant information across the organization, and then regularly adjust the rhythm of meetings as the environment changes
> - Gardeners set aside a significant portion of their time for coaching, sharing feedback, and shaping future careers for their team members in ways that will benefit the individuals and the organization
> - Gardeners proactively set up clear guardrails and then have the discipline to maintain an "eyes on, hands off" approach, allowing their team members the space to experiment, innovate, and grow

[^1]: Livingston, David, _What Kind of Leader Can Lead a Team of Teams? The 6 Principles of Leading Like a Gardene_, McChrystal Group, <https://www.mcchrystalgroup.com/insights/what-kind-of-leader-can-lead-a-team-of-teams-the-6-principles-of-leading-like-a-gardener/>, 12 October 2022.
