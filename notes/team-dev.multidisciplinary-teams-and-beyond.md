---
id: vd8kdh7chwcwqzfooi304p0
title: Multidisciplinary Teams and Beyond
desc: ''
updated: 1706447598003
created: 1706447428375
tags:
  - productivity
  - teams
---

Source: <https://www.infoq.com/articles/bridging-silos-overcoming-collaboration-antipatterns>

## Multidisciplinary Teams

Described as: members who may have separate but interrelated roles, and maintain their own disciplinary boundaries.

Teams are additive, not integrative.

## Interdisciplinary Teams

Described as: members who may blur some disciplinary boundaries, but still maintain a discipline-specific base.

Teams integrate closer to complete a shared goal.

## Transdisciplinary Teams

Described as: team members who share roles and goals. They share skills, allowing others to learn as well as acquire new skills.

Teams are more blended, and share objectives and many core skill sets as required to achieve their overall goal.
