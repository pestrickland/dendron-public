---
id: crxuq8pf448a4954ed350lb
title: Intrinsic Motivation
desc: ''
updated: 1685285092659
created: 1685284823654
---

According to Pink[^1], there are three elements of motivation that drive people who work in knowledge-based industries:

- A sense of [[productivity.purpose]]
- A sense of [[productivity.mastery]]
- A sense of [[productivity.autonomy]]

[^1]: Pink, D. _Drive_, Canongate, 2009.
