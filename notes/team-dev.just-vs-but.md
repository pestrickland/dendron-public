---
id: ovqb0j8bw3cdij9iksehemo
title: Just Vs But
desc: ""
updated: 1713634039767
created: 1713633963013
---

<https://substack.com/home/post/p-143698586>

The idea that some people are "justers", and others are "butters". "Just" suggests hurrying up and getting to an outcome, whereas "but" urges caution.
