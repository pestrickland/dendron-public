---
id: j4denxv13xhi9d7hgsfin2w
title: Including Everyone in Meetings
desc: ''
updated: 1688239966403
created: 1686134789798
---

- Proactively give less dominant people the floor
  - Don't pressure people though. Ask them if they agree or if they think there is anything that hasn't yet been covered
- Interrupt interruptions
  - Prepare phrases to use so as not to sound rude
- Take repeat offenders to one side outside of the meeting
- Acknowledge good contributions and praise them for it. Ensure the ideas aren't appropriated from others
- Ask domineering people to take notes
- Give everyone time to process any questions that need to be asked. Don't just give space for people who respond quickly
- Clean up as you go, summarising and agreeing any points and next steps
- Assign [[team-dev.direct-responsible-individual]] and rotate this role as required to share work
