---
id: pce3q6sbgjcuq660bc7tz4x
title: Resilience
desc: ""
updated: 1685286012939
created: 1685285906443
---

However good plans are, problems can still arise. An organisation needs to be **resilient** to be able to react to these problems and still succeed.

## To Research

- <https://www.mcchrystalgroup.com/insights/weekly-whiteboard-collective-resilience/>
