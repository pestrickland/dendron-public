---
id: fom80dv6ig6boutg0odxix2
title: Arcadia Capability
desc: ""
updated: 1709503501966
created: 1708896378474
tags:
  - capability
  - arcadia
---

## General Definition(s)

- [[definition.capability]]

## Operational Capability

This isn't certain yet, but I think an operational capability _need_ is what is required to achieve one or more [[missions|arcadia.mission]] or major goals of the stakeholders.

> Here, capabilities should be seen as quantified overall operational goals, results, services that are expected from end users. E.g. "localise", "follow route", "track"; or more extensively: “ability to detect/locate a given kind of target in such area in less than such time”.
>
> Assess qualitative but also quantitative metrics or parameters to quantify expected capabilities (e.g. precision of localisation).

In defence terms, it is important to consider any constraints likely to impact capability fulfilment across all DLODs.

If a capability _need_ is well defined, _gaps_ in the _current_ capability and existing systems or those on the market (such as from competitors) can be identified.

The tutorial provides this:

> What does it mean to have an "operational capability"? In the Arcadia method, we illustrate capabilities using "scenarios." These are sometimes referred to as "use cases" in other methodologies. [^1]

[^1]: <https://gettingdesignright.com/GDR-Educate/Capella_Tutorial_v6_0/OperationalAnalysis.html#OperationalActivities>
