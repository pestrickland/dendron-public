---
id: 2wu3qmprtc9sco67bp3rzvz
title: Likelihood Function
desc: ""
updated: 1723412168397
created: 1722803895289
---

Consider a patient $Y_i$ that has a mortality outcome of $\theta$. Each patient's outcome comes from a Bernoulii distribution:

$$
Y_i \sim B(\theta)
$$

The probability that patient $Y_i$ dies is $P(Y_i=1) = \theta$.

The probability density function can be written as (this is vector notation):

$$
P(\underset{\sim}{Y} = \underset{\sim}{y}|\theta) = P(Y_1 = y_1, Y_2 = y_2, ..., Y_n = y_n|\theta)
$$

Since each event is independent, the individual probabilities for each patient may be multiplied.

$$
\begin{align}
P(\underset{\sim}{Y} &= \underset{\sim}{y}|\theta) = P(Y_1=y_1)P(Y_2=y_2)...(Y_n=y_n|\theta) \\
&= \prod_{i=1}^n P(Y_i = y_i|\theta) \\
&= \prod_{i=1}^n \theta^{y_i}(1-\theta)^{1- y_i}
\end{align}
$$

Now, the likelihood function can be described. It is the PDF thought of as a function of $\theta$.

$$
\mathrm{L}(\theta|y)= \prod_{i=1}^n \theta^{y_i}(1-\theta)^{1- y_i}
$$

## Maximum Likelihood Estimate

This is the $\theta$ that maximises the likelihood, i.e.

$$
\hat{\theta} = \mathrm{argmax\ L}(\theta|y)
$$

To find the maximum value of theta, calculus is used. First, taking logarithms (the log of a product gives a sum, which is easier to work with):

$$
\begin{align}
\ell(\theta) &= \log \mathrm{L}(\theta|y) \\
&= \log \left[ \prod \theta^{y_i}(1-\theta)^{1-y_i}\right] \\
&= \sum \log \left[ \theta^{y_i}(1-\theta)^{1-y_i}\right] \\
&= \sum \left[ y_i \log \theta + (1-y_i)\log (1-\theta)\right] \\
&= \left(\sum y_i\right) \log \theta + \left(\sum (1-y_i)\right)\log  (1-\theta)
\end{align}
$$

Now find the derivative:

$$
\begin{align}
\ell'(\theta) &= {1 \over \theta} \sum y_i - {1 \over 1-\theta} \sum (1-y_i) \overset{\mathrm{set}}{=} 0 \\
\Rightarrow \frac{\sum y_i}{\hat{\theta}} &= \frac{\sum (1-y_i)}{1-\hat{\theta}} \\
\Rightarrow \hat{\theta} &= {1 \over n} \sum y_i
\end{align}
$$

## Central Limit Theorem

The central limit theorem allows a confidence interval to be approximated as:

$$
\hat{\theta} \pm 1.96 \sqrt{\frac{\hat{\theta}(1-\hat{\theta})}{n}}
$$

In general, there is something called the [[Fisher Information|stats.fisher-information]], where the MLE is approximately normally distributed with the mean at the true value of $\theta$ and the variance of one over the Fisher information at the value of $\hat{\theta}$:

$$
\hat{\theta}⩫ N\left(\theta, {1 \over \mathrm{I}(\hat{\theta})}\right)
$$

## General

In general, if the data are independent and identically distributed from a Bernoulli($p$), Poisson($λ$), or Normal($μ,σ^2$), then $\bar{x}$ is the MLE for $p$, $λ$ and $μ$ respectively.
