---
id: 0hpr6i2oydjnoksa3tpoj2g
title: Brain Dump
desc: ''
updated: 1669499361478
created: 1647789927931
tags:
  - research
  - management
---

The idea of a brain dump is to empty your brain. Write down all of your ideas, thoughts, things that need to be done, problems and worries. Once you have written those things down, your brain doesn't need to remember them and you can move on to do other things.

## Next steps

After a brain dump, you will have a list of things that need to be processed. In GTD terms, you would sort the items into projects and establish next actions. This is certainly a method but it makes each project quite linear and I have previously found it to be quite constraining.

## How often?

I haven't thought of a frequency for this yet. At the moment I've either done a brain dump when prompted by an article or podcast about productivity, or as a sort of pressure relief valve when feeling stressed. I'm sure it would be better to [[productivity.schedule]] a regular session though.
