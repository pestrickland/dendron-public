---
id: 8ojmcg3ay97u5krnsuwyasq
title: Misconceptions About Motivation
desc: ''
updated: 1688921048644
created: 1688920746851
tags:
  - productivity
---

- Motivation doesn't come before changing a behaviour, but rather after it
- Motivation is often the result of action, not the cause of it. In other words, getting started in some small way provides momentum and inspiration
- You don't need as much motivation once you've started changing behaviour. The friction is greatest at the beginning

<https://jamesclear.com/motivation#Common%20Misconceptions%20About%20Motivation>
