---
id: mplqmu6h1uhqcf3rv4wmdop
title: Personal Margins of Safety
desc: ""
updated: 1696104951210
created: 1696104858449
---

"Maintain a margin of safety. If your life is designed only to handle the expected challenges, then it will fall apart as soon as something unexpected happens to you. Always be stronger than you need to be. Leave room for the unexpected."
