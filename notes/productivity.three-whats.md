---
id: kvnwv3syifmt768wcqttvmq
title: Three Whats
desc: ""
updated: 1737807513513
created: 1737807271157
---

Use these three questions to invite connection and start work:

- What?
- So what?
- Now what?

See also [[productivity.five-whys]]

As an example:

- What: "What brought you here?"
- So What: "What do you hope to gain from being here?" (This is about people's expectations for what they will learn or experience.)
- Now What? "What will you do once you learn that?" Sometimes, this is best at the end, where I might ask the other person, "Based on this, what will you do?"

_Source: <https://www.jrothman.com/mpd/2025/01/how-the-three-whats-invite-people-to-a-human-connection>_
