---
id: a7tq8rq2mgus3rphpyqqian
title: Capability
desc: ""
updated: 1709421076113
created: 1709420576761
tags:
  - capability
---

Capabilities enable a [[mission|arcadia.mission]] (I think), in the same way that they enable [[goals|productivity.goals-projects]] to be achieved.

- [[definition.capability.nz-gov]]
- [[definition.capability.team-onion]]
- [[definition.capability.us-dod]]
- [[arcadia.capability]]
