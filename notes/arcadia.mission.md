---
id: juysiiy1f1lc82llniy63xm
title: Arcadia Mission
desc: ""
updated: 1709668168475
created: 1708896465162
tags:
  - arcadia
---

## Definitions

![[arcadia.mission.operational]]

![[arcadia.mission.system]]

## Notes

The [tutorial](https://gettingdesignright.com/GDR-Educate/Capella_Tutorial_v6_0/index.html) for the Capella tool/Arcadia method uses a "mission statement" like this:

> Our mission is to design a toy catapult to amuse a 4-year-old child safely creating an opportunity for parent and child to play together in a way that delights them both. [^1]

While the method does talk about missions, this seems to be used more at the system level. This is odd since the idea of a "mission" sounds very top-level to me, but maybe I will start to understand better.

The tutorial goes on:

> The Arcadia method recommends starting with a list of operational capabilities. For this example, we will map our mission statement to an operational capability, Entertain parent and child. [^1]

[^1]: <https://gettingdesignright.com/GDR-Educate/Capella_Tutorial_v6_0/OperationalAnalysis.html#Capabilities>
