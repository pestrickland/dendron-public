---
id: w36i8t8yv7ehzt1fy35ywk4
title: Who/What/When Matrix
desc: ""
updated: 1709591900533
created: 1709591786178
tags:
  - micropractice
---

## Streamlining Next Steps for Effective Meeting Outcomes

1. Create a matrix titled "Who | What | When" on a shared visual space accessible to all meeting participants.
2. Insert participants' names in the "Who" column. This focuses on the individuals responsible for action rather than the actions themselves.
3. Engage each participant to declare their actionable commitments and list these in the "What" column.
4. For each action, specify a completion date in the "When" column, committing in front of peers for accountability.
5. Review the matrix for balance and ask less committed members if they can enhance support or take on tasks.

A Cultural Micropractice taken from <https://www.human.pm/>
