---
id: 7eas8euhlrg7s8ms0j0ilwm
title: Team of Teams
desc: ""
updated: 1711904914401
created: 1670450126667
tags:
  - books
---

## Citation Info

McChrystal, Stanley A., Tantum Collins, David Silverman, and Chris Fussell. Team of Teams: New Rules of Engagement for a Complex World. Miejsce nieznane: Penguin Business, 2019.

## Annotations

![[reading.books.team-of-teams.one-best-way]]

![[reading.books.team-of-teams.deskilling]]

![[reading.books.team-of-teams.five-elements-of-management]]

![[empowerment.helping-ourselves]]

![[reading.books.team-of-teams.f3ea]]

![[reading.books.team-of-teams.complexity]]

![[reading.books.team-of-teams.big-data]]

![[reading.books.team-of-teams.uncertainty]]
