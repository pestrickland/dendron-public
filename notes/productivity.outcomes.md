---
id: r2cihjbpbdf4cj0vong6jvi
title: Outcomes
desc: ''
updated: 1700300301993
created: 1700299947556
tags:
  - productivity
  - capability
  - outcomes
  - team-onion
---

Emily Webber wrote an article discussing the concept of **outcomes**[^1]:

> An outcome is something that follows as a result or consequence. It is the value created and not how it was done.

This relates to the definition of [[definition.capability.team-onion]], defined in [[The Team Onion|reading.books.the-team-onion]].

[^1]: _People capabilities as outcomes_, <https://emilywebber.co.uk/people-capabilities-as-outcomes/>
