---
id: rmk43cqma8g6pgzu2k22avr
title: Reverse Mentoring
desc: ''
updated: 1707169490374
created: 1707125608364
tags: micropractice
---

## Aim

Bridge the gap between employees from different generations

## Area/skill

Behaviours

## Frequency

Weekly

## Method

- Pair up - match younger employees are paired with more experienced staff to share fresh perspectives, technological know-how, and current trends. This not only empowers younger employees but also helps senior staff stay up-to-date.
- Share stories - In team meetings, encourage them to share any things they’ve learned or experienced - keep it storytelling, not reporting.
- Occasional Nudge - Every now and then, just ask, "How’s the skill swap going?" in your regular catch-ups.

A Cultural Micropractice taken from <https://www.human.pm/>
