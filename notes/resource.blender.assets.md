---
id: ib70fvvw8s7edjknipyoioi
title: Assets
desc: ''
updated: 1720361714473
created: 1720360865017
tags:
  - blender
---

## Assets

- <https://www.3dassets.one>
- <https://www.poliigon.com> - Also an addon in Blender
- <https://www.blenderkit.com> - Also an addon in Blender
- <https://www.polyhaven.com>
- <https://www.hdri-haven.com>
- <https://3dwarehouse.sketchup.com>
- <https://www.turbosquid.com/>
- <https://www.ambientcg.com>
