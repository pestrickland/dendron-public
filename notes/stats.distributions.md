---
id: p3gv4r83jbkd2fpmir5sihs
title: Distributions
desc: ''
updated: 1721480844596
created: 1721384652627
---

## Bernoulli

![[Bernoulli|stats.distributions.bernoulli]]

## Binomial

![[Binomial|stats.distributions.binomial]]
