---
id: t7mpoustqkgpynane8uvxnu
title: Ideas
desc: ''
updated: 1686602556427
created: 1677452253854
---

A collection of more unconventional ideas (wisdom?).

## Easier Coordination vs Better Software

> When different parts of an organization need to coordinate, it seems like a good idea to help them coordinate smoothly and frequently. Don’t. Help them coordinate less — more explicitly, less often.

<https://jessitron.com/2021/08/02/better-coordination-or-better-software/>

- Use strong boundaries and put more effort into robust interfaces
- Treat other elements of an organisation like _different_ organisations that you can't know everything about
- This cuts dependencies and breaks the reliance on assuming everything has to be the same to be compatible
- There is more work involved in setting up the interfaces but less work in coordinating the disparate organisations
- Sometimes there are reasons for organisations to have different ways of doing things

## Work Out Loud

<https://gilest.org/archive/new-stuff-old-stuff>

> Working in the open means documenting what you do, as it happens. Writing and sharing lots of small updates over time.

## Re-invent the Wheel If It's Needed

- The first wheel was a potter's wheel; if we'd stopped there we'd still be walking
- Later wheels were re-invented by creating spokes
- A tyre band stopped wheels falling apart
- Wire wheels allowed aircraft to have undercarriage

Progress isn't a dirty word; it is important to be proportionate, but we shouldn't just stubbornly continue on with the same tools and processes if there might be a better way out there.

Taking the wheel a step further: the basic wheel can be put to work in various ways, but the role of the engineer is to optimise wheels for their specific applications. Some wheels have teeth and become gears; other wheels have extremely low friction; other wheels step in precise increments. They are all wheels, but each one is perfect for the job it was designed for, and less good in other roles.

## Connect the Dots Between the Operating Model and Reality

Corporate operating models contain everything that is needed to know what to do and how to do it. It is at a very high level, certainly, but that pure form can then be taken and applied to any number of specific applications. We should focus our goals to ensure that there is a good link between the local work being done and the overall vision of the company.

## Empowerment

If there is a problem with [[productivity.decision-bottlenecks]], it's not enough to bestow on the team the label of "empowerment". But an effort should be made to get the idea of [[leadership.pre-approval]] across. This way, uncertain people without the confidence to make decisions on their own may be able to reconcile what they're doing with the idea that they already have approval to act.
