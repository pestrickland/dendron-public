---
id: g03r0yybmbfq4vggye6lwuh
title: Mental Models
desc: ""
updated: 1697981801245
created: 1697977271396
tags:
  - learning
  - systems.thinking
  - systems.engineering
---

<https://www.modeltheory.org/about/what-are-mental-models/>

Mental models are:

- Sketches: outlines that represent pertinent information in an iconic form, e.g. a stick figure represents a person but not their fingers or organs
- Blueprints: coherent possibilities that could potentially occur in reality
- Diagrams: allow emergent insights to be learned by scanning them. They represent abstract concepts in a way that allows those insights to be seen
- Comic strips? They provide a simplified chronological representation of discrete and critical events

The assumptions of the theory are:

- Each model represents a possibility
- Models are iconic insofar as possible
- Models explain deduction, induction and explanation
- The theory gives a "dual process" account of reasoning
- The greater the number of alternative models needed, the harder it is
- The principle of truth: mental models represent only what is true
- The meanings of terms such as "if" can be modulated by content and knowledge
