---
id: u4a6yeihwz6tkis0u01wlyx
title: Whole System Thinking
desc: ""
updated: 1697982245784
created: 1697982179980
tags:
  - learning
  - systems.thinking
  - systems.engineering
---

Kauffman describes the concept of a complete and whole system:

> “...dividing the cow in half does not give you two smaller cows. You may end up with a lot of hamburgers, but the essential nature of “cow” — a living system capable, among other things, of turning grass into milk — then would be lost. This is what we mean when we say a system functions as a “whole”. Its behaviour depends on its entire structure and not just on adding up the behaviour of its different pieces.”
