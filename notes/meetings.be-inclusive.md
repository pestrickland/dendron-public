---
id: ft3eonv22z7iwbwkr465ihf
title: Be Inclusive
desc: ''
updated: 1686135211389
created: 1686133142189
tags:
  - meetings
  - productivity
---

Use [this play](https://www.atlassian.com/team-playbook/plays/inclusive-meetings) from Atlassian. (Also see other [[resource.tools.playbooks]])

See if anything related to shared understanding or a balanced team comes up in the [[resource.tools.team-health-monitor]] exercise.

## Why

- Many people have difficulty contributing and being heard in meetings
  - Remoter workers
  - Women
  - Introverts
- Women are far more likely to be interrupted in meetings
  - Their ideas are taken less seriously
  - Their ideas may even be co-opted by other teammates

## How

### Prep

- Write a detailed meeting agenda and send out at least 24 hours in advance
- Write agenda items as questions not topics
- Encourage people to come prepared
- Hold allies (men, extroverts) accountable for making space for other voices
- Be selective with attendees
- Budget enough time to cover everything on the agenda - it's better to finish early than run out of time

### Set the Stage

- Use equal seating to suggest equal value
- Make everyone feel welcome by introducing them and why they have been invited. E.g. the host introduces people rather than asking them to introduce themselves
- Set ground rules, e.g. no talking over each other. Call it out. Make use of mutes and hand-raising for remote meetings
- Explain structure. Have a conch, or a rubber chicken to squawk if someone hogs the floor
- Explain who is in charge of each item/section of the meeting
- Explain the goals of the meeting (should match the prep material)

### Tools to Use

![[Including Everyone in Meetings|resource.tools.including-everyone-in-meetings]]

### Wrap It Up

- Review key points (review the individual item reviews)
- Summarise/clarify/reiterate the next steps and [[team-dev.direct-responsible-individual]] for each
- [[Thank everyone|resource.tools.thank-people]] and highlight the value created

### Follow It Up

- Send a follow-up note capturing the key points, actions and [[DRIs|team-dev.direct-responsible-individual]]
- Solicit ideas that came up after the meeting - give introverted people other ways to share their ideas
- Keep tabs on actions. Assign someone to check in on actions at appropriate intervals after the meeting and before any follow-up discussions
