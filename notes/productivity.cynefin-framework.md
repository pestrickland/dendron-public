---
id: 0pdfj06fz25mgoqq73yrwpd
title: Cynefin Framework
desc: ""
updated: 1698492695649
created: 1698491617639
tags:
  - complexity
  - learning
  - systems.thinking
  - productivity
---

<https://en.wikipedia.org/wiki/Cynefin_framework>

This framework defines the following terms:

- Clear
- Complicated
- Complex
- Chaotic
- Confusion

They are shown forming a sort-of quadrant, although there are no axes.

## Clear

AKA "simple" or "obvious", this domain represents the known knowns. Cause and effect are clear, and the sequence of work can be _sense -> categorise -> respond_. It is best practice, but also at risk of being made too generic, too simple and fosters complacency.

## Complicated

This domain represents known unknowns. Understanding cause and effect requires knowledge and expertise and there are a range of right answers to a given problem. The framework for the right sequence of work can be _sense -> analyse -> respond_. It benefits from an operation of applying good practice.

## Complex

In complex domains there are unknown unknowns. There are no right answers and cause and effect can only be determined in retrospect. Patterns of work can emerge but it is experimental, and failure needs to be accommodated in such a way that it can be safe to fail. The sequence of work can be described as _probe -> sense -> respond_. It is an unpredictable and one that is associated with emergent areas of development rather than established ones.

[[See this for more information about complicated vs complex|scratch.2023.08.28.155037#complexity-management]]

## Chaotic

Cause and effect are unclear in this domain. Events are too confusing to apply a methodical knowledge-based approach. Instead, _any_ action is the first and only way to respond appropriately: _act -> sense -> respond_. In this case the response will hopefully transform the chaotic situation into a complex one.

## Confusion

This is a bad place to be because there is no way to sense what is going on. It must be broken out of and broken down to its constituent parts.FFF
