---
id: 3ud2kau1sv9llgecrevwup4
title: Normal Distribution
desc: ""
updated: 1732802623419
created: 1721940132704
---

AKA gaussian distribution.

$$
X \sim N(\mu, \sigma^2)
$$

Its density function is:

$$
f(x \vert \mu, \sigma) = \frac{1}{\sqrt{2\pi \sigma^2}} e^{-{1\over 2\sigma^2}(x-\mu)^2}
$$

## Expected Value

$$
E[X] = \mu
$$

## Variance

$$
\mathrm{Var}(X) = \sigma^2
$$
