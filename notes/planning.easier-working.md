---
id: suzlycft9z4vekvx6rq0xyr
title: Working Easier and Better
desc: ''
updated: 1697900989392
created: 1697900952413
tags:
  - productivity
  - planning
  - tips
---

## Non-intuitive Tips to Improve

Tips from [Johanna Rothman](https://www.jrothman.com/) about making your work easier and better[^1]:

- Move from "how much" to "how little" thinking
- Finish something cleanly every day
- Create an overarching goal for **all** substantive work

### How Much vs How Little

AKA push-planning, asking how much encourages highly optimistic thinking. You cannot create optimistic plans _if you care about meeting them_. If you plan to deliver, focus on thinking how little can be done so that work can still be effective (at reaching the overarching [[goal|productivity.goals-projects]]). Focus on reducing the backlog, managing work in progress, decreasing the time to complete any one task and to get feedback to act and improve on.

### Finish Cleanly

A tidy mind at the end of the day is lovely. When tasks are well defined (and small), this is quite achievable and rewarding - think about crossing off items on a todo list so that you have an empty list by the end of each day.

When work is less atomic (or cannot be split into [[inch pebbles|planning.inch-pebbles]]), it goes unfinished and can be harder to pick up again in future. Starting is often easier and more exciting than finishing.

### Have Goals for All Work

[[Projects have goals|productivity.goals-projects]]. Sometimes a project only emerges after completing some of the work, but it is still a project and needs a goal.

- Why are you the right person to do the work?
- Who will benefit from the outcome?
- What are the benefits?

Answering these questions explains the value you provide.

[^1]: <https://www.jrothman.com/newsletter/2023/10/three-not-so-intuitive-tips-to-make-your-work-easier-and-better/>
