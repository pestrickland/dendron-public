---
id: pqab29g0mxv8g9jaad2c0xh
title: Rambling
desc: ''
updated: 1684413046139
created: 1684412880467
---

From a discussion with Adam:

> i'm trying to think of a walking/rambling analogy about give them the destination and the tools to get there. You don't need to build the path (although you can) but markers along the way or the tools to find the way work. And maybe a few "don't go in this field there are bulls".
>
> here is a map of the landscape and a compass. Don't forget your sandwiches and we'll meet you for a drink at the pub at the end. Last one there will pay the bill.
>
> but maybe one of those satellite images so you have to keep you're eyes open for emerging things that weren't initially visible
>
> You could show how there is a well-trodden path that is probably going to be easy to follow and painless, but there's nothing to stop you taking a different route depending on your needs. There may be some boundaries, but we are not going to tell you each and every step to take
>
> you are free to get there how you want but you are aware of teh law and taking a bulldozer to get to the finish or walking through the centre of someone's house is likely to get you into trouble so don't be an idiot
>
> i think you've got something there to explain how things are. You are free to make your own way and you may find a more attractive route to you
>
> i want to walk along the river whereas someone else would rather risk it across the cliff edge as it gets them there quicker
>
> it does also help me explain those times we don't know how we are getting there (there is currently no path) but giving people to freedom to find out. There is no path but you know where we're going, let's get started and share how you're getting on. "I've got my feet wet in a river so going back a step", "come this way wet foot i've made it halfway by hopping on these boulders i dropped in"
>
> i would, it abstracts the idea enough to think simply about it. We trust everyone knows how to walk safely and so set them free. Hopefully they are inspired by the option to find their own route and share the exciting things they have seen. If they want a path they are in the wrong group or they are going to be waiting at the bus stop for a while until someone else makes a path.
>
> it makes it sound like it's in their hands or they can hang around twiddling their thumbs while others have fun and possibly just be a burden as they are making other people pull them along
>
> it would be a good thread as part of a change management thing. You could have symbols for things like maps, compass etc to symbolise the tools and steps
