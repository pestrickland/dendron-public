---
id: ggnkx1zcmpiwy8718wtx9rk
title: Group Decision Making
desc: ''
updated: 1690662842949
created: 1690662623290
tags:
  - facilitation
---

## To Reach a Consensus

- Bring a list of curated options for discussion
- Ask people to decide in private and then share their thoughts in turn

## To Minimise Politics

- Number each option and facilitate anonymous voting
- Use dot voting

## To Capture Rationale

- Ask participant to write _why_ they want to go a certain way when they make a selection or vote
- Display reasons alongside the ideas being discussed/chosen
