---
id: 5im4ffsc6daysddv6jb7w5m
title: Types of Meeting
desc: ""
updated: 1714914445937
created: 1691063581183
tags:
  - productivity
---

## Calendly

<https://calendly.com/blog/types-of-meetings>

1. Status update meetings
2. Decision-making meetings
3. Problem-solving meetings
4. Team-building meetings
5. Info-sharing meetings
6. Innovation meetings

## BetterUp

<https://www.betterup.com/blog/types-of-meetings>

1. Decision-making meetings
2. Problem-solving meetings
3. Team-building meetings
4. Brainstorming meetings
5. One-on-one meetings
6. Quarterly planning meetings
7. Check-in meetings

## Lucid

<https://www.lucidmeetings.com/meeting-types>

### Cadence

1. Team Cadence
2. Progress Check
3. One-on-One
4. Action Review
5. Governance Cadence

### Catalyst

1. Idea Generation
2. Planning
3. Workshops
4. Problem Solving
5. Decision Making

### Learn and Influence

1. Sensemaking
2. Introductions
3. Issue Resolution
4. Community of Practice
5. Training
6. Broadcasts

## Indeed

<https://ca.indeed.com/career-advice/career-development/types-of-meetings>

1. Introductory meetings
2. Private meetings
3. Progress update meetings
4. Planning meetings
5. Information sharing meetings
6. Decision-making meetings
7. Training sessions
8. Problem-solving meetings
9. Innovation and prainstorming meetings
10. Team building meetings
