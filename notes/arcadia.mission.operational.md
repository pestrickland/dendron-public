---
id: ydocdrbxj0yjd3u52rl4no2
title: Operational Mission
desc: ""
updated: 1709668171507
created: 1709668006312
tags:
  - arcadia
---

## Operational Mission Definition

> A high-level goal to which one or more operational entities should contribute, and which is likely to influence system definition or usage
