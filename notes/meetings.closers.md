---
id: chm7sttxhg72ms0723f2ymi
title: Closers
desc: ''
updated: 1686506330468
created: 1686413241998
---

[This article from Jackie Colburn](https://jackiecolburn.medium.com/if-you-open-a-meeting-with-icebreakers-end-with-these-wrap-up-equivalents-for-better-closure-8d5739930bbe) is quite interesting.

## When You Need Everyone to Align While They're Still in the Room Rather than Hashing Out Important Details via a Lengthy Email Thread

Use the [[Horizons|meetings.closers.horizons]] method at the end of a strategic workshop, design sprint or many-hour/day meeting as a way to organise what will happen next, and who will be responsible for doing it when.

This activity invites the entire group to identify and contribute next steps, action items and critical tasks. It also asks individuals to self-assign ownership over their tasks. By collaborating and allowing group members to decide which action items they'd like to champion, there should be a lot more buy-in which will lead to less work falling through the cracks and being missed.

## When You Want to Help People Feel More Included and Connected to the Work

After clearly stating the next steps, leave the door open for additional thoughts and conversation (really?!). It might be better to offer this as an offline option, e.g. ask people to email you with their thoughts or other ideas when they think of them.

This option aims to make next steps feel more like an open conversation, driving continued dialogue in an honest fashion. Things get missed and people get tired in an intense meeting, so offering this as an option afterwards provides a space for people to contribute in others ways once they've had more time to think.

## When You Need Specific Input on How to Make an Initiative/Launch/Roll-Out Successful among Key Constituents

After stating the next steps, ask people to make anonymous suggestions to help make the initiative successful. This will get more honest thoughts, and will help to uncover the true thoughts of the team. Ask questions like:

- What's going to be important to consider as we roll out the plan?
- What questions do you think we should anticipate?
- What will people need to know or feel for this to be a success?
- What blockers might we need to navigate?

This technique should help to shed light on institutional challenges - people will more candidly explain how there is little chance of _X_ going successfully because of _Y_ structures/processes getting in the way. People's concerns reported candidly might help to uncover inertia too - after a successful, positive meeting, some people might not want to be the naysayer but have a valuable and genuine concern about how something could get in the way.

With these contributions gathered, the facilitator needs to summarise them and present the themes back to the team for its consideration.

## When You Want to Check in with Where the Group Has Landed and See How People Are Feeling

Ask people how they feel at the end. If you use [[meetings.icebreakers]] at the start, e.g. "in one word, how do you want to feel at the end of this session," you can mirror that quite neatly.

It might seem a bit soon, if you go to the effort of asking people what they hope to achieve and then an hour or two later ask them how they now feel. If the session was quite intense, however, it is suggested that it's a nice point to end on. It also holds the facilitator accountable, but presumably if expectations were appropriately managed and that people don't expect the world all at once. If people don't feel that they go everything they hoped for, it is still valuable in identifying where extra work is needed.

## When You Want People to Commit to Next Steps and Stay Accountable

Ask people to select a bite-sized commitment they will make based on the information shared during the meeting. This can be an individual or group commitment.

People get to make a declaration in front of their peers. It also helps keep up the momentum built in the meeting as people go away to focus on the next steps. If these commitments (they sound a lot like actions) are written down, that's good. Going further, the team could pair up into "accountability buddies" to encourage and support each other after the session.
