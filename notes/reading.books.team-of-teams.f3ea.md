---
id: j745q28dx5assdadkjfa845
title: F3EA
desc: "F3EA: Find—Fix—Finish—Exploit—Analyse"
updated: 1708879653054
created: 1708879422139
---

Noted in _[[reading.books.team-of-teams]]_ Chapter 2: Clockwork

This is a sequence of operations that was used in Iraq, as described in _[[reading.books.team-of-teams]]_:

- Find
- Fix
- Finish
- Exploit
- Analyse
