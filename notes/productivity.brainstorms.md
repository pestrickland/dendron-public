---
id: baqyqgiqudx5b1g7lxmnfbw
title: Brainstorms
desc: ''
updated: 1658060786119
created: 1658059130229
tags:
  - productivity
---

>Decades of subsequent research shows that group brainstorming results in fewer novel ideas than individuals brainstorming separately and later pooling their ideas.[^1]

One possible reason for this is "production blocking", where your idea cannot be introduced at the time due to someone else talking. When you do finally get a chance to raise it, it is lost since the discussion has moved on. This is particularly difficult for introverts.

Another reason is an apprehension of being judged by your peers or superiors. "Evaluation apprehension" makes you reluctant to speak up, especially if in a negative sense to the direction that other, louder members of the team or meeting are going.

Worse still, these traits compound, so the more people involved in a brainstorming session, the fewer ideas are generated.

## Async Brainstorms

Rather than thinking that people have to be together to generate ideas, you can encourage more thoughtfulness by setting up async brainstorms. Essentially people go away, think by themselves (when it's convenient for them, in familiar surroundings etc), and then the ideas are collated and shared. 

I guess you could have a session where everyone gets together to share their ideas, but that's only one step removed from doing the actual brainstorming session. Better would perhaps be a way to share and discuss ideas in an async way after everyone has had a chance to brainstorm by themselves.

[^1]: _Want a more creative team? Research says to work async_, https://async.twist.com/how-async-combats-groupthink/