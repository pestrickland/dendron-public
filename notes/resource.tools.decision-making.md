---
id: u7sw6oo57rwa97fu3r1ocoy
title: Decision Making
desc: ""
updated: 1707645704363
created: 1707645704363
---

Untools [^1] has a few resources about different tools to be used for decision making. It also has an app that allows you to create:

- Eisenhower Matrix
- Decision Matrix
- 2x2 Custom Matrix

[^1] <https://untools.co>
