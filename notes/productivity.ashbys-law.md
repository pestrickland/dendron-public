---
id: x9a8nxq7bhk08dxrfvme8oo
title: Ashby's Law
desc: ""
updated: 1706452164592
created: 1706452155888
---

> a control system must have at least as much variety as the system it is managing.
