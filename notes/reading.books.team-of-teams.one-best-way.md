---
id: s5c5lbq8ye02qotoc1zcs9p
title: One Best Way
desc: ""
updated: 1708878459337
created: 1708798595902
---

Noted in _[[reading.books.team-of-teams]]_ Chapter 2: Clockwork

The idea that there is only one **right** way to do things that led to the factory system:

> They could not all be right, [[Taylor|reading.books.team-of-teams.frederick-taylor]] thought - there must be one best way

This is perhaps a more positive view of Taylor, in that he is trying to demonstrate the value of a method rather than just imposing his will:

> He would not **make** them work harder - he would **show** them that it could be done

Unfortunately, these ideas have persisted:

> Taylor's foundational belief – the notion that an effective enterprise is created by commitment to efficiency, and that the role of the manager is to break things apart and plan "the one best way" – remains relatively unchallenged.
