---
id: 9m3ruid8vnbzr40ygkg6o6k
title: Double Diamond Solution Discovery
desc: ""
updated: 1691588393074
created: 1680431786388
tags:
  - productivity
---

The idea is that there are four stages to find a solution to a problem, and that the process involves alternating divergence and convergence of thought:

- Discover (the problem)
- Define (the problem)
- Develop (the solution)
- Deliver (the solution)

I explored something similar to this when constructing the initial principle behind the disturbance chain. The definition here gives a more robust explanation.

<https://marcabraham.com/2023/03/24/discovery-discipline-book-review/>

However, the article where this is described also states that it is a very fragile process, with some teams embarking on futile explorations that lead to know meaningful discovery, and others defaulting to suggestions from senior people. It goes on to describe a refinement of this process in a book called _Discovery Discipline_[^1]. The new discovery discipline method consists of three components:

- Seven steps to follow to prompt different but complementary perspectives on the problm to be solved
- A list of possible activities to conduct at each step
- Short deliverables for the end of each step

The seven step process is named "focused":

- Frame (define the project ambition and deliver a success metric)
- Observe (identify the primary use case and deliver a first use case)
- Claim (come up with the narrative positioning and deliver a launch tweet!)
- Unfold (choose the key points in the experience and deliver 5 touchpoints)
- Steal (reuse existing solutions and deliver golden nuggets (of wisdom?))
- Execute (build a working version and deliver a "happy path")
- Decide (evaluate the quality of the solution and deliver a go/no-go verdict)

There seems to be a lot of material available in the book[^1] and it might be worth putting on my list.

[^1]: Charvillat, Tristan and Guyot, Rémi, _Discovery Discipline: The Radical Method to Master Product Discovery_, Thiga, 2022
