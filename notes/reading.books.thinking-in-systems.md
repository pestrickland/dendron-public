---
id: gfb4t0sa66elmt30t5y8scd
title: Thinking in Systems Reading Notes
desc: ""
updated: 1685477842911
created: 1668979738202
---

## Citation Info

<!-- Comment: you can replace this biblatex section with a citation template in whatever style you tend to use, if you don't use Pandoc or something similar for generating citations. I currently edit these citations by hand. -->

```biblatex
@book{${1:link},
  author    = {Donella H. Meadows},
  title     = {Thinking in Systems – A Primer},
  year      = {2008},
  volume    = {},
  edition   = {1},
  series    = {},
  number    = {},
  publisher = {Chelsea Green},
  address   = {},
  pages     = {},
  note      = {},
  origdate  = {2008},
  isbn      = {978-1-60358-055-7}
}
```

<!-- Comment: You can change any of the below text. Make it your own! -->

## Definitions

### Function vs Purpose

Things have functions; people have purposes.

## Parts of a System

### Elements

### Interconnections

### Purposes

## Stock

> A stock is the memory of the history of changing flows within the system.

```mermaid
flowchart LR
  id1((Source))-->|inflow|id2[Stock]
  id2-->|outflow|id3((Sink))
```

Flow is the action that changes the stock.

There are all sorts of different stock that can be modelled in this way.

```mermaid
flowchart LR
id1(("🌧"))
id2(("🌊"))
id3[Reservoir]
id4(("☀️"))
id5(("🚰"))

id1-->|Rain|id3
id2-->|River Flow|id3
id3-->|Evaporation|id4
id3-->|Discharge|id5

```

## What was the thesis? Main argument?

## Were there any side arguments that were significant?

## How does this text fit within the literature? Historiography?

(What is it adding to the arguments already out there? does it challenge dominant trends in the literature?)

## What methodology did the author use?

## Evidence? What kinds of sources did the author use?

## Organisation? How did the author organise the flow of the argument and narrative?

## Critiques? Do you see any problems? Praise?

## Epistemology? (this is less important based on your field)

## Anything else of interest you would want to remember? Maybe something that will be useful for your dissertation
