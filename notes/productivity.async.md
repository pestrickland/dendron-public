---
id: ac6iqbb4drbiocpyxr6mu35
title: Async
desc: ''
updated: 1669499316065
created: 1653134966347
tags:
  - research
  - leadership
  - management
  - teams
---

- Expect replies within 24 hours
- Document as you go, e.g. when meetings _are_ needed, you not only share slides but allow for input from all involved to be made in the open. It would be possible for the issues to be discussed and resolved ahead of the actual meeting

## Time blocking

[Source](https://async.twist.com/email/a10b1c3c-7ee6-4927-bd45-df3bb6596e79/)

1. Open your work calendar for next week.
2. Identify at least a one 1-hour chunk of time each day and physically block it off so your team can't schedule over it (and you don't forget about it).
3. When the time comes, close out of your communication apps, identify one important task you want to accomplish, and work on just that task until your time block is done.
4. Bonus: Let your teammates know you're running an experiment with how you structure your time so they know what to expect (and maybe get them thinking about trying it too).

## Brainstorms

![[Async Brainstorms|productivity.brainstorms#async-brainstorms]]
