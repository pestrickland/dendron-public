---
id: jepfo64whq9xz63h6tbpzgv
title: Goals, Projects, Hobbies and Progress
desc: ""
updated: 1709420796669
created: 1673984627711
tags:
  - productivity
---

- A **goal** without a **project** is a **dream**
- A **project** without a **goal** is a **hobby**
- A **goal** with a **project** is **progress**

Do [[capabilities|definition.capability]] enable goals?
