---
id: dgrekuxvx6vbqtd95uwmvk0
title: Five Elements of Management
desc: ""
updated: 1711903338621
created: 1708799998568
---

Noted in _[[reading.books.team-of-teams]]_ Chapter 2: Clockwork

Five elements of management were identified:

- Planning
- Organising
- Command
- Coordination
- Control
