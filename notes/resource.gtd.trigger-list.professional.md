---
id: un89k63ynzuzpgq8c4i4h3h
title: Professional
desc: ""
updated: 1706450245203
created: 1706449936407
---

## Projects

- started, not completed
- Projects that need to be started
- “Look into” projects

## Commitments/promises to others

- boss
- partners
- colleagues
- subordinates
- others in organization
- other professionals
- customers
- other organizations

## Communications to make/get

- calls
- emails
- voicemails
- faxes
- letters
- memos

## Writing to finish/submit

- reports
- evaluations
- reviews
- proposals
- articles
- marketing
- material
- instructions
- summaries
- minutes
- rewrites and edits
- status reporting
- conversation and communication tracking

## Meetings

- upcoming
- need to be set or requested
- need to be de-briefed

## Read/review

- books
- periodicals
- articles
- printouts
- websites
- blogs
- RSS feeds

## Financial

- cash
- budget
- balance sheet
- P&L
- forecasting
- credit line
- payables
- receivables
- petty cash
- banks
- investors
- asset management

## Planning/organizing

- goals
- targets
- objectives
- business plans
- marketing plans
- financial plans
- upcoming events
- presentations
- meetings
- conferences
- travel
- vacation

## Organization development

- org chart
- restructuring
- lines of authority
- job descriptions
- facilities
- new systems
- change initiatives
- leadership
- succession planning
- culture

## Administration

- legal issues
- insurance
- personnel
- staffing
- policies/procedures
- training

## Staff

- hiring
- firing
- reviews
- staff development
- communication
- morale
- feedback
- compensation

## Systems

- phones
- computers
- software
- databases
- office equipment
- printers
- faxes
- filing
- storage
- furniture
- fixtures
- decorations
- supplies
- business cards
- stationery
- personal/electronic
- organizers

## Sales

- customers
- prospects
- leads
- sales process
- training
- relationship building
- reporting
- relationship tracking
- customer service

## Marketing/promotion

- campaigns
- materials
- public relations

## Waiting for

- information
- delegated projects/tasks
- pieces of projects
- replies to communications
- responses to proposals
- answers to questions
- submitted items for response/reimbursement
- tickets
- external actions needed to happen to continue or
- complete projects...(decisions, changes, implementations, etc.)
- things ordered

## Professional development

- training
- seminars
- things to learn
- things to find out
- skills to develop or practice
- books to read
- research
- formal
- education (licensing, degrees)
- career research
- resume

## Wardrobe

- professional
