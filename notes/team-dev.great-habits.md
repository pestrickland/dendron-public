---
id: menki9152iepbxwf25xotjw
title: 20 Great Habits of High-Performing Leadership Teams
desc: ''
updated: 1697390217685
created: 1697389133675
tags:
  - productivity
---

<!-- markdownlint-disable MD029 -->

_Source: <https://jurriaankamer.medium.com/20-traits-of-stellar-leadership-teams-12213d3ce8e>_

## Deciding

1. Ask [clarifying questions][1] before reacting
2. Ask ["is it safe-to-try?"][2] instead of "is it perfect"?
3. Use a different process for [reversible and irreversible decisions][3]
4. Clarity on which types of decisions need group consent and which don’t

## Improving

5. Spend monthly recurring time for reflecting on and improving the team
6. Team works [_on_][4] the organisation: running experiments to improve its Operating System
7. Feedback is flowing freely between members
8. Member’s learning goals are shared openly to help each other achieve them

## Strategy

9. Members have participated and [co-created the strategy][5]
10. The strategy contains [clear trade-offs][6], clarity on what NOT to do
11. Uses cycles of ['90 day outcomes'][7] that they are working against together
12. Reviews steering metrics to know if their shared work is progressing

## Meetings

13. Their [meeting routine][8] drives the work forward
14. Uses a tool like Trello, Notion, or Planner to capture projects and actions
15. The role of the meeting facilitator rotates between members
16. Uses asynchronous workflow: chat/audio/video for updates and unblocking

## Behaving

17. Equal talking time; [everyone makes proposals][9]
18. Disagreement is seen as an opportunity to explore [multiple truths][10]
19. Members name feelings and [hold space][11] for processing tension
20. Models the behaviour shift they’d like to see in the rest of their organization

The red thread: participation, co-creation, equality, adult-adult, accountability, [consent][12], continuous change.

[1]: https://medium.com/the-ready/create-an-empowered-organization-using-participatory-governance-b5dd2ed20161
[2]: https://jurriaankamer.medium.com/accelerate-your-decision-making-with-this-powerful-sentence-38a58bb8db36
[3]: https://jurriaankamer.medium.com/jeff-bezos-advice-on-decision-making-will-help-you-accelerate-and-reduce-costs-e9014c0ac7f0
[4]: https://www.linkedin.com/posts/jurriaankamer_as-a-leader-you-may-feel-that-the-mindset-activity-7058717595343974400-k7sg?utm_source=share&utm_medium=member_desktop
[5]: https://medium.com/the-ready/how-adaptive-strategy-happens-f62674445634
[6]: https://medium.com/the-ready/even-overs-the-prioritization-tool-that-brings-your-strategy-to-life-e4f28f2949ac
[7]: https://medium.com/the-ready/how-adaptive-strategy-happens-f62674445634
[8]: https://medium.com/the-ready/how-to-facilitate-the-best-meeting-your-team-will-have-this-week-763f31b6d7d
[9]: https://medium.com/the-ready/create-an-empowered-organization-using-participatory-governance-b5dd2ed20161
[10]: https://medium.com/the-ready/when-your-operating-system-is-designed-for-equity-9aece597c145
[11]: https://medium.com/the-ready/building-your-teams-safety-ecosystem-6746a37a79d8
[12]: https://medium.com/the-ready/create-an-empowered-organization-using-participatory-governance-b5dd2ed20161
