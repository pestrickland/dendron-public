---
id: rp5q28v93wg2krvn2gku6ng
title: Marginal Probabilities
desc: ''
updated: 1721393327491
created: 1721393030735
---

Marginal variables are those in the subset of retained variables. [^1] They are described as "marginal" due to them being written in the margins of a table after summing.

[^1]: Marginal distribution, <https://en.wikipedia.org/w/index.php?title=Marginal_distribution&oldid=1182695783> (last visited July 19, 2024).
