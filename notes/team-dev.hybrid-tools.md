---
id: ysw09q6m2h2qu775z6dvfjn
title: Hybrid Tools
desc: ""
updated: 1716579211774
created: 1716579001266
---

[This post](https://emilywebber.co.uk/a-framework-for-thinking-about-team-memory-joining-up-and-serendipity-in-hybrid-organisations/) from [[people.emily-webber]] gives some ideas about how to encourage and improve:

- Team Memory
- Joining up
- Serendipity

All of these are more difficult in hybrid organisations (not totally remote organisations or those that are fully office-based, but those where people are working in a variety of ways, locations and modes).
