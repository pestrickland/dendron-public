---
id: rros0devz1x5yqx919ixiyd
title: Giles Turnbull
desc: ""
updated: 1710624775758
created: 1710624566872
---

<https://gilest.org/index.html>

Writes about [[productivity.weeknotes]] and [[productivity.presentations]].
