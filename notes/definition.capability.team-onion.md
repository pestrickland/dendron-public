---
id: b5vwdooz7sfsydkhs3qfztt
title: Team Onion Capability Definition
desc: ""
updated: 1709420493642
created: 1691153489068
tags:
  - productivity
  - capability
  - outcomes
  - team-onion
---

_The Team Onion_ defines **capability** as:

"The application of knowledge, skills and experience to achieve an outcome."

An outcome is defined [[here|productivity.outcomes]] as "something that follows as a result or consequence. It is the value created and not how it is done."
