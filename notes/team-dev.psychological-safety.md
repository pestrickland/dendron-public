---
id: axyzscct4aa4pehb73fcrtr
title: Psychological Safety
desc: ""
updated: 1723395255977
created: 1722842487810
---

Look at [Psychological Safety Does Not Equal "Anything Goes"](https://amycedmondson.com/psychological-safety-%E2%89%A0-anything-goes/) for a good depiction of what psychological safety can enable in different environments.

It is not wrapping people in cotton wool, but about enabling people to work and learn in a high-performing environment, including having difficult conversations where necessary. Just look at the picture in the link!
