---
id: 3iqrs1i9pj0zipnnz2g08g2
title: Trust
desc: ''
updated: 1685284344383
created: 1679867226513
tags:
  - ideas
  - productivity
---

<https://www.rubick.com/high-trust-low-trust-organizations/>

The idea here is that trust is good for an organisation. Low trust brings low retention, blame culture and a work ethic based around command and control. High trust organisations focus on learning, autonomy and outcomes rather than activities and end up with high retention.

See also [[freedom]].

Trust is also a key tenet from McChrystal's _Team of Teams_ model[^1]:

> Teams must develop a culture rooted in trust, built by high levels of benevolence, competence, and reliability, and maintained through transparent communication.

[^1]: Livingston, David, _What Kind of Leader Can Lead a Team of Teams? The 6 Principles of Leading Like a Gardene_, McChrystal Group, <https://www.mcchrystalgroup.com/insights/what-kind-of-leader-can-lead-a-team-of-teams-the-6-principles-of-leading-like-a-gardener/>, 12 October 2022.
