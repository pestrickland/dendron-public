---
id: a10jb47cckcked6o80igg7n
title: Conditional Probability
desc: ""
updated: 1725830946753
created: 1725830946753
---

$$
P ( A \vert B ) = \frac{P(A  \cap B)}{P(B)}
$$
