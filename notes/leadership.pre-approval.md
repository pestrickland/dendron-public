---
id: smaksv9y2hs2sa6xe56z75h
title: Pre-Approval
desc: ''
updated: 1686507237326
created: 1684094286725
---

_See [[freedom]] and [[productivity.trust]]._

If there is a problem with [[productivity.decision-bottlenecks]], it's not enough to bestow on the team the label of "empowerment". But an effort should be made to get the idea of pre-approval across. This way, uncertain people without the confidence to make decisions on their own may be able to reconcile what they're doing with the idea that they already have approval to act.

Of "thousands of people" being asked whether they prefer "being told what to do", "complete freedom" or "freedom within guidelines,"

> Very few [people] like to be told what to do. A few anarchists like complete freedom. But most want a framework within which they have freedom.[^1]

[^1]: <https://www.corporate-rebels.com/blog/create-trust-and-freedom>
