---
id: czke0f1fyfjy2gxgunye71c
title: Leadership is Language
desc: ""
updated: 1708799615386
created: 1668377598868
---

## Citation Info

L. David Marquet, _Leadership is Language_, Penguin Business, 2020.

## What was the thesis? Main argument?

- Replace **reactive** language of _convince_, _coerce_, _comply_ and _conform_ with **proactive** language of _intent_ and _commitment to action_
- Replace language of _prove and perform_ with language of _improve and learn_
- Replace language of _invulnerability and certainty_ with _vulnerability and curiosity_
- "Intent-based leadership®"
- Idea of a playbook of leadership, with 6 new plays:

  - Control the clock instead of obeying the clock
  - Collaborate instead of coercing
  - Commitment rather than compliance
  - Complete defined goals instead of continuing work indefinitely
  - Improve outcomes rather than prove ability
  - Connect with people instead of conforming to your role

- Balance _doing_ with _thinking_ (performing and proving is doing; growing and improving is thinking)

## Were there any side arguments that were significant?

The team of the _El Faro_ were using the old plays of _continue_ and _comply_.

## How does this text fit within the literature? Historiography?

(What is it adding to the arguments already out there? does it challenge dominant trends in the literature?)

## What methodology did the author use?

## Evidence? What kinds of sources did the author use?

## Organisation? How did the author organise the flow of the argument and narrative?

- Six leadership plays we have been programmed for
- Six leadership plays we want to use as replacements

![[Red Work vs Blue Work|reading.books.leadership-is-language.red-work-blue-work]]
