---
id: yl44fiplpw0uavjootkx7eb
title: Frederick Winslow Taylor
desc: ""
updated: 1708878217912
created: 1708876859305
---

From [Wikipedia](https://en.wikipedia.org/wiki/Frederick_Winslow_Taylor):

> Frederick Winslow Taylor (March 20, 1856 – March 21, 1915) was an American mechanical engineer. He was widely known for his methods to improve industrial efficiency.[^1] He was one of the first management consultants.[^2] In 1909, Taylor summed up his efficiency techniques in his book The Principles of Scientific Management which, in 2001, Fellows of the Academy of Management voted the most influential management book of the twentieth century.[^3] His pioneering work in applying engineering principles to the work done on the factory floor was instrumental in the creation and development of the branch of engineering that is now known as industrial engineering. Taylor made his name, and was most proud of his work, in scientific management; however, he made his fortune patenting steel-process improvements. As a result, scientific management is sometimes referred to as Taylorism.[^4]

- [[One Best Way, as described in _Team of Teams_|reading.books.team-of-teams.one-best-way]]

[^1]: _New York Times (The)_ (March 22, 1915). "F.W. Taylor, Expert in Efficiency, Dies". _The New York Times_. Vol. 64, no. 20876. p. 9 (col. 5). Retrieved March 14, 2008 – via [TimesMachine](https://nyti.ms/3CDh4Wq).
[^2]: _Wall Street Journal (The)_ (June 13, 1997). "[Frederick Taylor, Early Century Management Consultant](https://web.archive.org/web/20080514135125/http://www.cftech.com/BrainBank/TRIVIABITS/FredWTaylor.html)". p. A17. Archived from the original on May 14, 2008. Retrieved May 4, 2008 – via Wayback Machine.
[^3]: Bedeian, Arthur G.; Wren, Daniel Alan, PhD (Winter 2001). "[Most Influential Management Books of the 20th Century](https://faculty.lsu.edu/bedeian/files/most-influential-management-books-of-the-20th-century.pdf)" (PDF). _Organizational Dynamics_. **29** (3): 221–225. doi:[10.1016/S0090-2616(01)00022-5](https://doi.org/10.1016%2FS0090-2616%2801%2900022-5). Retrieved March 12, 2017 – via Louisiana State University. ISSN [0090-2616](https://www.worldcat.org/search?fq=x0:jrnl&q=n2:0090-2616); doi:[10.1016/S0090-2616(01)00022-5](https://doi.org/10.1016%2FS0090-2616%2801%2900022-5); OCLC [5198509376](https://www.worldcat.org/oclc/5198509376).
[^4]: Epstein, Marc J. (1996). "[Taylor, Frederick Winslow (1856–1915)](https://archive.org/details/histaccounting00chat/page/578/mode/2up)". In Chatfield, Michael; Vangermeersch, Richard (eds.). _[History of Accounting: An International Encyclopedia](https://archive.org/details/histaccounting00chat/page/578/mode/2up)_. New York: Garland Publishing. pp. 579–580. ISBN [9780815308096](https://en.wikipedia.org/wiki/Special:BookSources/9780815308096). Retrieved August 29, 2022 – via Internet Archive.
