---
id: kmrosy6qc0lmiq6dvyvjvjf
title: Blue Work
desc: ''
updated: 1708799208912
created: 1708799004725
---

Blue work is focused on exploring options. Thinking calmly people are encouraged to explore how a solution can be developed or improved
