---
id: mjuzxk4zlkaw56i26uk5pln
title: Meetings
desc: ''
updated: 1685203908652
created: 1657361978161
---

Here is a post that explains how meetings fit in with [[async working|productivity.async]]. Rather than remove all meetings, the meetings that you do have are more important and should be more valuable.

<https://async.twist.com/flipped-meeting-model/>

> Meetings should be the exception in people’s workdays, not the rule.

## Modified Bloom's Taxonomy

The post describes what it calls a modified version of [Bloom's Taxonomy](https://en.wikipedia.org/wiki/Bloom%27s_taxonomy). I haven't explored the original version yet, but in the post, the idea is an inverted pyramid related to knowledge gain at different stages of an education process. The most knowledge is gained by the student studying before a lecture, then this is enhanced by the lecture or class, and finally after the lecture, the creative effort is maximised. It's not clear if the width of the pyramid depicts time/effort/cost or cognitive effort, but efficiency is still one of the critical elements of the concept. I think this is similar to the 70/20/10 idea of training, where the majority is on the job, then learning from others and finally courses.

## Basic concept

Use the precious time of working together to solve the things best suited to that time, solving well-defined problems or making decisions. Don't start researching or thinking generally. Don't use meetings simply to convey information.

> No more daily standups where everyone takes turns reciting their to-do lists.

## Flipped meetings

Let's start with the purist option and see what we could actually achieve.

1. Meeting time should be reserved for meaningful interaction, not conveying information.
2. Focus on having fewer meetings first, then figure out how to make the ones you do have better.
3. Everyone should come prepared with the information and context to participate. Twist threads, Almanac documents, Loom videos, and Pitch presentation recordings are all great ways to provide context asynchronously before a meeting.
4. No one should feel obliged to attend a meeting where their active participation isn’t needed.
5. Meetings should be limited to fewer than eight people — the upper limit beyond which meaningful participation falls off and people become more guarded and less candid. Five or fewer is even better.
6. Outcomes should always be documented afterward for people who couldn’t or didn’t need to attend.

## But what about the social side?

The reading I've done so far suggests that this should probably be dealt with separately.
