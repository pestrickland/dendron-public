---
id: w014c70te5falehan74at5q
title: Organic Accountability
desc: ""
updated: 1671136102874
created: 1671134439407
tags:
  - productivity
  - leadership
---

Try to encourage more ownership and accountability at meetings by ensuring each session ends with people clear on what they need to do (and are expected to do) by when. It could be by the time of the next meeting (e.g. a weekly cadence of meetings), or it could be by x days after the meeting.

The tricky part of this is how to establish this more accountable culture without imposing it on people. We don't want to see people becoming more and more used to being spoon-fed instructions; we want to encourage organic growth in these qualities. How?
