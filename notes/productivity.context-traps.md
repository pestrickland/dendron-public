---
id: 0ub8fr39eogscos0cj4k4pb
title: Context Traps
desc: ""
updated: 1736103996602
created: 1736103602729
tags:
  - cutlefish
---

From [The Beautiful Mess](https://cutlefish.substack.com/p/tbm-329-deep-vs-broad-context-traps).

1. Pay close attention to when your pattern-sensing radar goes off and you start congratulating yourself for seeing something you've seen before. Why? This is often a signal that you might be overgeneralizing.
2. Write down where you've worked and for how long, compare those contexts, and ask yourself, "Where did I form my foundational beliefs about how things work, and do they still hold here?"
3. Do a quick inventory of your "justs" (e.g., "to succeed, you just need trust"). Ask yourself: Where did I form that belief? How might it manifest differently in other contexts?
4. Write down some "first principles" you believe apply everywhere (e.g., "transparency builds trust"). Then, ask: What factors must be paired with these principles to make them work in different contexts?
5. Think back to a company you worked at that was successful. Reflect on the factors you usually attribute to its success, then run a thought experiment: What other factors—environmental, structural, or even luck—might reasonably explain part of that success?
6. Ask yourself, "Do the things I try work because I make them work in a given context or because they are inherently the right thing to do everywhere?"
7. Ask yourself, "Do I keep arriving at the same diagnosis because it's genuinely helping or because it's the pattern I'm most comfortable recognizing? What might I be missing about this specific context?"
