---
id: pdy0c79aaio37ptkp3nd057
title: Purpose
desc: ""
updated: 1685285259842
created: 1685285018576
---

According to McChrystal's model[^1]:

> Teams across an organization must have a clear "north star", a collective understanding of _where_ we are going, _why_ we are going there, and _how_ we are going to get there.

[^1]: Livingston, David, _What Kind of Leader Can Lead a Team of Teams? The 6 Principles of Leading Like a Gardene_, McChrystal Group, <https://www.mcchrystalgroup.com/insights/what-kind-of-leader-can-lead-a-team-of-teams-the-6-principles-of-leading-like-a-gardener/>, 12 October 2022.
