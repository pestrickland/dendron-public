---
id: i9f9bo5habppe8y8ka44goa
title: Introduction to PyMC
desc: ''
updated: 1720739610939
created: 1720644779629
tags:
  - learning
  - statistics
  - bayes
  - lang.python
---

These notes are based on reading the _Introductory Overview of PyMC_ [^1]. At the same time, I was watching a tutorial on using Python for statistical modelling [^2].

## Notes

- Designed to allow development of Bayesian inference solutions to problems
- "Markov Chain Monte Carlo" sampling enables this inference but requires gradient-based algorithms that are difficult to calculate with complex models
- PyMC generates such gradients

## Questions

- What is a "probabilistic model" in this context?

[^1]: _Introductory Overview of PyMC_, <https://www.pymc.io/projects/docs/en/latest/learn/core_notebooks/pymc_overview.html>
[^2]: _Christopher Fonnesbeck - Introduction to Statistical Modelling_, <https://youtu.be/TMmSESkhRtI>
