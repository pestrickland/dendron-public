---
id: 1zxd0ljemxnn3tfwjv6yd8u
title: Whiteboards
desc: ''
updated: 1696272433641
created: 1696264342624
---

## Initial Search

With Google Jamboard going away, I looked at free online whiteboard options. There are three that I found:

- Lucidspark
- Mira
- Figjam

Additionally there is the built in whiteboard on Webex and the shareable OneNote to consider as options.

### Lucidspark

Looks nice, includes template for dot voting, but any collaboration (including read only) requires a login, which means that people would need to create a free account. Annoying.

### Figjam

Similar to Lucidspark, you need to log in to be able to collaborate.

### Mira
