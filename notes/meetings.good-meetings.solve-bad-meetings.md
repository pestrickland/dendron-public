---
id: 2nlfyinzeujm8e45wuub1z9
title: Solve Bad Meetings
desc: ''
updated: 1699905223948
created: 1693841560874
tags:
  - micropractice
---

## Solving bad meetings

1. What's your default meeting length? 30, 45, or 60 minutes? Why? Not every issue deserves the same amount of time so pick meeting duration on a per meeting basis.
2. Schedule meetings in increments of 5 minutes. Tell the meeting organiser to provide a reason why they need more than 4 increments (20 minutes)
3. Make meeting prep a requirement not an option. Give people things to read or do before the meetings. If they don’t, kick them out.
4. If someone is more than 2 minutes then the last person to the meeting, they put £10 into the coffee fund.
5. Use an egg timer. Set the time. When it goes off, you’re done. If you didn’t finish, blame the timer.
6. The meeting organiser is obligated to send a short summary with action items within 10 minutes of the end of the meeting
7. If you’re not adding value to a meeting, leave. You can read the summary later.

A Cultural Micropractice taken from <https://www.human.pm/>
