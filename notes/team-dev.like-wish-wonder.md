---
id: pxjc00ocppedk6sk3oa8i9l
title: 'I Like, I Wish, I Wonder'
desc: ''
updated: 1689004481662
created: 1689003016994
tags:
  - productivity
  - team-building
---

This arrived in the 10 July edition of _Culture micro-practice_:

A simple and effective way to provide constructive feedback on a particular subject or topic. It encourages a balanced approach by **highlighting positive aspects** (I like), **areas for improvement** (I wish) and **raising questions or suggestions** (I wonder).

- **I like:** Focus on the positives. Start by highlighting the aspects you appreciate or find praiseworthy. Be specific and provide examples to support your statements. This helps create a positive tone and ensures the recipient understands what they are doing well
- **I Wish:** Suggest improvements. Identify areas that could be enhanced or changed. Offer constructive criticism and suggestions for improvement. Focus on actionable and specific recommendations rather than vague statements. Be considerate and provide reasoning for your suggestions
- **I Wonder:** Raise questions or seek clarification. This part involves asking questions or seeking additional information. It allows you to express curiosity, seek further clarification, or generate discussion. These questions can help the recipient reflect on their work and provide valuable insights.

<https://www.human.pm/culture-micro-practices>
