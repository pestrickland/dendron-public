---
id: usvivkfzjj6a7ov65yh8ubo
title: Stats
desc: ''
updated: 1721395919271
created: 1721395919271
---

This set of notes was put together in an attempt to learn more about probability, statistics and, in particular, Bayesian inference.

This work included taking a course on Coursera, reading and watching tutorials about the Python packages PyMC, Arviz and Bambi.
