---
id: 1z1a3j401l342j8p6dm2xgz
title: System Mission
desc: ""
updated: 1709668119868
created: 1709668051958
tags:
  - arcadia
---

## System Mission Definition

> A high-level goal to which the system should contribute.
