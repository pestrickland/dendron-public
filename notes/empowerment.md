---
id: nlg226de33od5an5dsfx0rv
title: Empowerment
desc: ""
updated: 1714914552133
created: 1685285517420
---

Here be dragons. "Empowerment" is often nothing more than a lazy trope to pass responsibility on to employees without allowing them the [[productivity.autonomy]] or means to actually make their own decisions.

Perhaps something like [[team-dev.prioritisation]] acts, or can act, to provide a sense of empowerment? At least it could, if done well, allow people to make their thoughts heard when it comes to what is or isn't important enough to do right away.
