---
id: 8syc9t642mt42hwxw56vvp9
title: Prioritisation
desc: ""
updated: 1714914875944
created: 1714913555047
---

[This post](https://blog.crisp.se/2024/04/16/marcuscastenfors/the-prioritization-problem) describes a failed approach to prioritising tasks using parameters; each candidate scored the highest score for each parameter and so no ranking could be achieved.

It suggests that there is a deeper problem at play: strategy.

> If the strategy, the direction, is clear, prioritization becomes straightforward. The opposite is very much true.

Consider asking yourself two problems:

1. What are the most important business and customer challenges to solve in the next 12 months?
2. Do teams understand why these challenges are important and are they tangible enough for them to gauge how they can meaningfully contribute to solving them?

Often, a [[leadership.leader]] will state that the answer to 1. is clear, but be unclear about 2. Jason Yip was said to name this an "air sandwich" problem. In other words, the strategy is clear to those who work with strategy, but not to those who work on tasks. The work that is part of the day-to-day task-worker's role is well understood by them, but not to the strategy people. The idea is to _fill the sandwich_ with meaningful problems to solve.
