---
id: gbwfm41ztjlwzywbeljua5f
title: Perfection vs Progress
desc: ""
updated: 1724666960801
created: 1724666587895
---

> Perfection is the enemy of progress.[^1]

The popular alternative when a group is in disagreement is a principle called [_disagree and commit_](https://en.wikipedia.org/wiki/Disagree_and_commit), but it is suggested here that it should be thought of instead as "_disagree and commit to learn_".

Once perfection has been discarded and a team has committed to make progress, there is still [[productivity.tension]] to deal with.

[^1]: <https://www.antmurphy.me/newsletter/healthy-and-unhealthy-tension>
