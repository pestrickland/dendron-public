---
id: kmmx2lzob1wmov4elix1k01
title: Leadership
desc: ""
updated: 1670450112642
created: 1666523227406
tags:
  - research
  - leadership
  - management
  - teams
---

## Researching technical leadership

Not all technology experts are leaders. Clearly.

> Technical leadership is the ability to supervise a team of technical experts while making decisions related to engineering and software development. It entails overseeing and leading the process of developing technological solutions. [^1]

Two important elements are stated to be skills and personal traits.

- Skills comprise both technical and management skills
- Traits are mostly related to experience, habits, etc

The following breakdown of skills and traits [^1] could be considered for more research and maybe learning if any gaps are identified.

### Skills

#### Technical

- Technical guidance: translating a high-level vision/architecture to team members. Educating the team on the reasons behind decisions that were made. Ensuring the team effectively implements the resulting plan
- Technical decisions: making the final decision to ensure the team progresses, whilst not stifling individual opinions or suggestions
- Coding: not specifically applicable but essentially maintaining the technical skills that you rely on your team to do, e.g. ITE
- Code reviews: aka TA. Providing review using a coaching approach to educate the team members with the why as well as the what and how
- Operational excellence: being responsible for the team's performance

#### Management

- Project management: planning the execution of the team's work and managing its time. Can be looking from a more long-term high-level perspective
- Crisis management: being calm under pressure, in control and able to steady the ship
- Problem solving: doing more than just identifying problems but implementing solutions too
- Personality: building meaningful relationships to establish trust with various stakeholders

### Traits

#### Holistic and broad perspective

- Thinking more broadly than just the technical solution
- Considering the perspective of customers and the company
- Also considering the impact on the individual team members

#### Organisational and actionable steps

- Breaking things down into smaller _actionable_ chunks
- Defining a framework within which the completion of the chunks will contribute towards the ultimate solution

#### Know thy team

- Each member has strengths and weaknesses that must be known by the leader
- The leader assigns tasks to benefit the team's growth
- The leader mentors and inspires the team

#### Communication

Poor communication is a common source of conflict. As asynchronous communication increases so does the importance of good communication. Good communication involves:

- Articulating what a particular solution should look like
- Asking whether all team members have the same picture of the solution
- Encouraging the sharing of any different solutions proposed by team members
- Ensuring that the team has the same understanding of the agreed solution
- Handling any conflicts within the team
- Being empathetic to the team but still acting rationally
- Ensuring that the actions of the team contribute to its growth and also the overall goal of the company

#### Innovation

- Experimenting with new ideas and ways of working
- Considering unconventional approaches whilst also considering potential risk
- Taking risks where appropriate and deliberate
- Employing [[critical thinking|productivity.critical-thinking]]
- Identifying any problems with novel approaches early
- Continuously improving tools, techniques and processes

#### Resilience

Dealing with burnout is an important responsibility:

- Identifying burnout in the team and oneself
- Creating a stress outlet for members of the team
- Trying to address stress with the team by looking for creative ways to reduce it
- Offer regular opportunities to socialise, de-compress and build bonds
- Connecting with other leaders to share problems and ideas
- Fostering a positive environment as much as possible

[^1]: <https://linearb.io/blog/technical-leadership-guide>

## Books

#books

- [[Leadership is Language|reading.books.leadership-is-language]]
- [[Thinking in Systems|reading.books.thinking-in-systems]]
- [[Team of Teams|reading.books.team-of-teams]]

## Courses
