---
id: h44rubychnafpmiioes74hd
title: Exponential Distribution
desc: ""
updated: 1732802612449
created: 1721598957215
---

- Exponential distributions represent events that occur at a particular rate
- The exponential is the waiting time between events, e.g. waiting times for a bus
- Used when events come from a poisson process

$$
X \sim \mathrm{Exp}(\lambda)
$$

$\lambda$ is known as the rate parameter. The exponential

$$
f(x \vert \lambda) = \lambda e^{-\lambda x } \quad \text{for } x\ge 0
$$

## Expected Value

$$
E[X] = {1 \over \lambda}
$$

## Variance

$$
\mathrm{Var}[X] = {1 \over \lambda^2 }
$$
