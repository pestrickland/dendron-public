---
id: rsvf32eaiv1z2l4nfvchqix
title: Bayes' Theorem
desc: ""
updated: 1725903781636
created: 1725830822927
---

$$
P (A \vert B) = \frac{P(B \vert A)P(A)}{P(B)}
$$

Or, making use of the [[law of total probability|stats.fundamentals.total-probability]]

$$
P( A \vert B) = \frac{P(B\vert A)P(A)}{P(B\vert A)P(A) + P(B \vert A')P(A')}
$$
