---
id: i9u92sa05ssd254d96kkq3h
title: Leader
desc: ""
updated: 1714914673446
created: 1714914639718
---

What is a leader? What should they be? What should they not be?

Not:

- [[leadership.micromanagers]]
- [[leadership.umbrella-managers]]
