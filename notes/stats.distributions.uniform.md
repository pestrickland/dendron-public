---
id: 5wwxc087u6rx4gv3zjb3adq
title: Uniform Distribution
desc: ""
updated: 1732802629093
created: 1721491623280
---

## Case 1

$$
X \sim \mathrm{U}[0,1]
$$

$$
\begin{aligned}
f(x) &= \begin{cases}
1 &\text{if } x \in [0,1] \\
0 &\text{otherwise}
\end{cases} \\
&= 𝟙_{\{0 \le x \le 1\}} (x)
\end{aligned}
$$

## Case 2

The uniform distribution may not be distributed on the interval 0–1. It could be larger, or the interval could be unknown.

In general then,

$$
X \sim [\theta_1 , \theta_2]
$$

and

$$
f(x \vert \theta_1, \theta_2) = {1 \over \theta_2 - \theta_1} 𝟙_{\{ \theta_1 \le x \le \theta_2 \}}
$$

### Expected Value

$$
E[X] = \int_{-\infty}^{\infty} xf(x)\mathrm{d}x
$$

In general,

$$
\begin{aligned}
E[g(x)] &= \int g(x)f(x)\mathrm{d}x \\
E[cX] &= cE[X] \\
E[X + Y] &= E[X] + E[Y] \\
\end{aligned}
$$

If $X\perp Y$, i.e. _X_ is independent of _Y_, then $E[XY]=E[X]E[Y]$.

### Variance

It's uniform, so there is no variance.
