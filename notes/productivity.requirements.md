---
id: ltvquian6ufvsajhafesqdu
title: Requirements
desc: ''
updated: 1670278199614
created: 1670277287267
tags:
  - software-design
  - requirements
  - productivity
  - ideas
---

How to capture and manage requirements is a perennial interest of mine. I found this note, and couldn't quite work out what I was trying to do with it:

![Sketch from a few years ago](assets/images/ink.png)

```mermaid
flowchart TD

id0[Requirements collection] --> id1[Requirement Document] --- id2[Requirement Document] --- id3[Requirement Document]


id0 --> id4[Project Document]
id5[Projects Collection] --> id4
id4 --- id6[Project Document]
```

I reckon it's possibly more of a software architecture than the more systems-engineering-based ideas I've been working on lately; more tools than process.
