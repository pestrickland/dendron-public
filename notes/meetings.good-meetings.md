---
id: o5z38kjnah9xquwgd586yxy
title: Good Meetings
desc: ""
updated: 1714914282594
created: 1678262318820
tags:
  - productivity
  - meetings
  - strategy
---

This article[^1] tries to make the case that the widespread dismissal of meetings might be incorrect.

## Moving to Knowledge Work

First coined in 1966 by Peter Drucker[^2] to pinpoint the difference of 20th Century work that rendered previous styles of management obsolete. One aspect of this was the shift from "just" doing things right, to doing _the right things_.

### Dignity

In manual labour, it is suggested that dignity comes from the tangible output of the work itself. For knowledge work, where do you find dignity?

For knowledge work, with nothing tangible, dignity is linked to "dignity as knowers". But it's not just how much we know or what we know, but "how good we are at judging the truth of uncertain things". This rings a bell with much of what we do at work. Drucker[^2] measures this by looking at the outcomes of decisions. But it is also possible that good judgement is a worthwhile aim in itself. How does this relate to meetings? Let's see...

Finally on this topic is the suggestion that in a healthy workplace, the whole system promotes higher-quality knowledge production and that the output is greater than anyone could produce alone.

Saying that meetings aren't work rejects the dignity of knowledge work and of your colleagues as knowers. It's better to honour our meeting spaces.

## Collective Intelligence

Organisations often appear to fail to be able to learn. One book about organisational dysfunctions has sections such as "How Professionals Avoid Learning" and "Defensive Reasoning and the Doom Loop"[^3], which illustrates a reluctance for both individuals and organisations to learn and develop. Self-preservation acts to prevent change.

"It's significantly more complex for _groups_ of people develop whole new theories-in-use and behaviors to better carry out the organization’s purpose."[^1]

## What works for learning?

- A good individual knowledge worker has information and experience in the domain they’re making decisions about
- A great knowledge worker has a solid practice of learning, demonstrably improving in effectiveness over time
- The best knowledge workers have a social reflective practice to acquire more diverse input, do a better job of interpretation, and actively find errors in thinking[^1]

So what makes a "practice of learning"?

## Truth as a Concept

- Truth is not a solid concept, i.e. there are varying standards of proof and views on what is "true enough" to act on. Related to group think, which in itself isn't a bad thing, since consensus is essential
- Jeff Bezos and Amazon worked on a "true enough" principle, acting when about 70% of the desired information was held. This was called "Bias for Action"[^4]

## True Enough as a Choice

"We have a choice whether to back off from our conflicting views and needs, living in a grey goo of agreeable compromise, or to embrace them and integrate our perspectives and needs into something new"[^1]. Can we use this idea to make something better than existed before? Doing this is what the article considers the _real work_, and it laments that it usually has to fit into the margins of "productive" work. So is that why it's all about the meetings? The meetings are seen as a waste of time because they are not _productive_, but is it possible that it's only in meetings that this sort of work can really be achieved?

> The tech industry suffers from a deep association of work with individual productive toil, and that just isn’t what knowledge work is. In addition to being social, knowledge work is also uncertain and messy. Moreover, it’s hard to make knowledge production happen reliably. There are better and worse ways, but in general, meaning-making argues with agendas and likes to break through timeboxes.⁴
>
> Knowledge-making is unruly. You can’t know up-front which things are tangents. Even one distracted participant can sour a batch. Forming new knowledge requires you to be open to whole people, not just the facet of their expected role. It can also be ephemeral; if you don’t work to keep them, good thoughts vanish again. As Samo Burja says, new knowledge is often produced in small, intellectually illegitimate spaces. If we want to get better at it, we need — individually and collectively — to be comfortable with necessary mess and improve over time with social reflective practice.

## Shared Meaning-Making

### How to Make Knowledge Together

### How to promote culture change that values social meaning-making and reflective practice

> Recognize that it takes active resistance for interpretation spaces to thrive in tech orgs. There is such a strong pull in our work systems to decompose work that it’s borderline revolutionary to center integration and meaning instead. It is likely to be uncomfortable and probably won’t be directly rewarded.
>
> Notice the conditions under which you are a not good as a knowledge-maker (e.g. early mornings, after too many other meetings). Avoid collaboration under those circumstances, because it reinforces the devaluation of collaborative spaces.
>
> Positively reinforce the meaning-making parts in meeting spaces. Example: “this conversation was valuable — I’d hadn’t realized we were using different definitions!”
>
> Negatively reinforce the one-way or mechanical tasks in shared spaces Examples: shift simpler communciations to written or recorded media, ringfence reading time for others if necessary, and most importantly, lead by actually reading what others communicate to you async.
>
> Be mindful of zoom levels and different cognitive tasks. Although people may be passively gathering signals on different levels, it’s difficult and disorienting when the actual conversation shifts levels and lenses too fast. It helps to develop a richer understanding of different sub-activities of knowledge production.
>
> Resist the urge to fill in the hunger for meaning with lists and tasks. Conversations with these as a primary focus are the empty calories of a working life. Fair play if they exist to provoke the useful conversations, though.
>
> Shift personal development away from project management mechanics like backlog management or forecasting and towards knowledge and communication topics like facilitation, collaborative decision-making, effective writing.
>
> Get inspired by learning collectives. There are many, but I’ll highlight [Decolonial Futures](https://decolonialfutures.net/) as doing especially interesting and radical work.

[^1]: [Meetings _are_ the work](https://medium.com/@ElizAyer/meetings-are-the-work-9e429dde6aa3)
[^2]: P. Drucker, _Landmarks of Tomorrow_
[^3]: C. Argyris, _On Organizational Learning_, Wiley, June 1999.
[^4]: [Amazon Leadership Principles](https://amazon.jobs/content/en/our-workplace/leadership-principles)
