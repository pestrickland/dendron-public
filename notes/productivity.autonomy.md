---
id: 273b6zmt0tped56vhnz9r5y
title: Autonomy
desc: ''
updated: 1683385791173
created: 1683385521857
---

Effective teams need to be autonomous, and for this to be effective, they need to be cross-disciplinary.[^1]

[^1]: https://jchyip.medium.com/key-practice-aligned-autonomous-cross-disciplinary-teams-d73c1cddc352

- Teams that constantly need to ask for permission and direction are slower
- Teams that have all the information, skills and authority they need are faster
- Autonomous teams are not just taking orders
- Autonomous teams tell you what they've been doing, rather than asking what to do
