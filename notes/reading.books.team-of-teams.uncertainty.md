---
id: uwyw9da92nmfugouhnnbsh4
title: Uncertainty
desc: ""
updated: 1711904668904
created: 1711904554406
tags:
  - complexity
---

Noted in _[[reading.books.team-of-teams]]_ Chapter 3: From Complicated to Complex

How do we manage uncertainty? Even with [[reading.books.team-of-teams.big-data]]:

> We have moved from data-poor but fairly predictable settings to data-rich, uncertain ones.
