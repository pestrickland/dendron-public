---
id: 4a0w79saqo18t8aq8loe7st
title: Writing
desc: ''
updated: 1653821824311
created: 1653745893169
tags: productivity
---
- Don’t get it right, get it written
- "all the first draft needs to do is exist."
- You write to figure out what you have to say. You edit to figure out how to say it well
