---
id: ocvuo5dfhgy0usy5ihoy2ly
title: Creative Process
desc: ""
updated: 1685895623478
created: 1685894332402
---

Cited by James Clear[^1], James Webb Young defined five steps in the creative process.[^2]

1. Gather new material
2. Thoroughly work over the materials in your mind
3. Step away from the problem
4. Let your idea return to you
5. Shape and develop your idea based on feedback

Contrary to popular belief, creativity is learned as much as anything else. Some people are more disposed to be creative than others, but it's not an excuse.

You should give yourself permission to create rubbish. Just start creating as you would practise anything else to improve. You will have to sift through a lot to find a true nugget of value, but it will happen the more you practise.

Create consistently. Don't wait for inspiration. Schedule it.

Finish something, and naturally this requires you to start something too.

Have enough self-compassion that you don't judge yourself harshly. Don't let judgement prevent delivery.

Be accountable for what you create. Share it publicly.

[^1]: _Creativity: How to Unlock Your Hidden Creative Genius_, <https://jamesclear.com/creativity>
[^2]: Young, J.W. _A Technique for Producing Ideas_, McGraw-Hill Education, originally published 1940.
