---
id: ix42lrj2pq6wplpexdp2e8u
title: Decision Making
desc: ""
updated: 1696105539500
created: 1684093215918
---

## Bottlenecks

The top-down decision-making model provides lots of control but creates a bottleneck. Once a team is conditioned to require a decision to be made by someone else, and the number of decision-makers is low, a bottleneck results and the team's output is constrained.

Similar bottlenecks exist in the desire to ensure consistency, **if** there is little [[productivity.trust]]. It should be possible to maintain quality and consistency, without pouring everything into the bottleneck, by fostering a culture of quality and robust [[productivity.autonomy]].

## Conviction

"Many people have a hard time making decisions because they don't know what is important.

When you have a clear mission and you are completely sure what is important to you, most decisions become easy. Once you're fully committed, you don't need rules for how to spend your time. It's obvious which decision to make. It's clear what to prioritize.

Many people don't need productivity or time management advice. They need conviction."
