---
id: 5u3lplfivmn39x38lrvztg8
title: Describe Your Goal as a Tweet
desc: "What would be written in the tweet of the future about your company?"
updated: 1676124449571
created: 1676124316947
tags:
  - productivity
---

Source:<https://medium.com/informationartist/try-this-vision-building-exercise-to-provoke-deep-thinking-63f168deca55>

> In 140 characters or less, describe what is your vision towards … (Add your goal here).
