---
id: ccatylrgminhv50n168p4jr
title: Freedom
desc: ''
updated: 1685177074293
created: 1683997005550
---

<https://www.corporate-rebels.com/blog/create-trust-and-freedom>

> Very few [people] like to be told what to do. A few anarchists like complete freedom. But most want a framework within which they have freedom.

This also introduces the idea of [[pre-approval|leadership.pre-approval]].
