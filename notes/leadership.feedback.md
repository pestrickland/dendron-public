---
id: egkumuc9qe1g4pdvfatpp39
title: Feedback
desc: ""
updated: 1717332441091
created: 1683995263791
---

Ideas for improving the culture of feedback.

- <https://hbr.org/2023/04/how-managers-can-make-feedback-a-team-habit>
- <https://medium.com/rbg-digital/normalising-feedback-b8fef8c43ca2>

## Feedback is Hard

- People want to be liked
- They don't want to upset or offend
- People fear critical feedback will be taken personally
- There is concern that feedback will contribute to stress to someone who is self-critical
- People may doubt the validity of their feedback, i.e. imposter syndrome
- They may think their opinions lack authority or do not matter
- There is a challenge to deliver feedback effectively, finding the right time, the right approach and the best language

## Feedback Plays a Crucial Role in Shaping a High-Functioning and Happy Team

- Feedback makes our work more impactful
- Feedback helps individuals to grow
- Feedback ensures we stay on track
- Considerate feedback is a great gift to give

## Practise Radical Candour

<https://www.radicalcandor.com/>

- Tell people you want to practise radical candour
- Start by asking for feedback rather than giving it out
- Create go-to questions for inviting feedback rather than asking if anyone has anything to say
- Embrace uncomfortable silences when asking for feedback; wait at least 6 seconds before prompting
- Always reward feedback
- Act fast
- Use the [["situation, behaviour, impact" framework|productivity.sbi-framework]]
- Don't personalise feedback
- Make feedback short and share it promptly
- Praise in public; challenge in private

## Ineffective Feedback

> Ineffective feedback is feedback the other person…
>
> - already knew, so they don’t do anything differently
> - has no clue how to put into action
> - feels forced to act on because they’re afraid of what happens if they don’t
> - thinks is inaccurate
> - is not motivated to do anything about
> - dismisses because they think you’re out to get them
> - finds confusing so they disregard
> - misunderstands and does something that isn’t what you intended
> - acts on but with great resentment toward you
> - acts on only to get such a poor outcome that they learn to do the opposite

From [_The Looking Glass: Holding Up the Mirror_](https://lg.substack.com/p/the-looking-glass-holding-up-the) by [Julie Zhou via Substack](https://lg.substack.com/)

## Excellent Feedback

Care for the other person:

- You must genuinely want to help the other person
- Try to see the other person as a flawed human being just like you
- See your role as helping them to reach their potential

Have courage:

- Have courage to speak plainly and be truly honest:

  - "When you talk like that, you sound to me like a follower and not a leader". This is better than
  - "I'd like to see you work on your communication"

Have a high standard of excellence:

- If your bar isn't high enough, you won't be able to give feedback to help the other person improve
- If they are already great, superior to you, and have impact in all they do, raise the bar - there will be more that they could be doing
- Imagine what the best version of them in 2-3 years could look like

From [_The Looking Glass: Holding Up the Mirror_](https://lg.substack.com/p/the-looking-glass-holding-up-the) by [Julie Zhou via Substack](https://lg.substack.com/)
