---
id: bqrzpixbr17y2wltoh8f1fa
title: Getting Started
desc: ''
updated: 1688921531192
created: 1688921059695
tags:
  - productivity
---

Try not to waste time on other parts of the process; try to automate the early stages of the change you're making so you can focus on your goals.

- Schedule motivation by planning when your behaviour changes or activities are going to take place ahead of time
- The classic example is to put out your running clothes before going to bed, so you don't have to decide whether or not to run the next day
- Actually write and follow a todo list
- Follow a routine/ritual before you start

<https://jamesclear.com/motivation#How%20to%20Get%20Motivated>
