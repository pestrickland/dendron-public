---
id: n6ll4uytu6bm9p5k87nu14y
title: Big Data
desc: ""
updated: 1711904648087
created: 1711904310624
tags:
  - complexity
---

Noted in _[[reading.books.team-of-teams]]_ Chapter 3: From Complicated to Complex

Big data alone can't solve our problems:

> Even if we covered the Earth in a lattice of sensors spaced one foot apart, and even if every one of these sensors provided flawless readings, we would **still** not know whether it would rain in a month, because the small spaces between those sensors hide tiny deviations that can be of massive consequence.

> Data-rich records can be wonderful for explaining **how** complex phenomena **happened** and how they **might** happen, but they can't tell us when and where they **will** happen.

This is an important point to remember:

> Gaining understanding is not always the same as predicting.

And this all leads to [[reading.books.team-of-teams.uncertainty]].
