---
id: 0c59pco9i2fo8grtw3s1hae
title: Five Whys
desc: ""
updated: 1737807409897
created: 1737807335120
---

Use the five whys technique to explore the underlying factors of a particular problem.

<https://en.wikipedia.org/wiki/Five_whys>

See also [[productivity.three-whats]]
