---
id: 5pcv3g4w7iwg9tay7gy6th4
title: Safe to Fail
desc: ''
updated: 1696109289974
created: 1696109229713
---

## Create a safe-to-fail environment with the ability to undo

> People need a safe-to-fail environment to learn in, where mistakes can be made and risks taken. This may mean allowing people time to learn with the close support of others or being put in real-life, low-risk situations that they can undo. Don’t put them in a position where mistakes will break a live service that is business-critical. This might be through low-risk projects or deliberate practice.[^1]

[^1]: <https://emilywebber.co.uk/learning-knit-reminded-learning/>
