---
id: vixd5c3n4194ic4b70s4xf0
title: Personal Trigger List
desc: ""
updated: 1706449916983
created: 1706449023928
---

## Projects

- Projects started, not completed
- Projects that need to be started
- Projects – other organisations
  - service
  - community
  - volunteer
  - spiritual organisations

## Commitments/promises to others

- spouse
- partner
- children
- parents
- family
- friends
- professionals
- Items to return

## Communications to make/get

- calls
- emails
- faxes
- cards
- letters
- thank-you’s

## Upcoming events

- birthdays
- anniversaries
- weddings
- graduations
- outings
- holidays
- vacation
- travel
- dinners
- parties
- receptions
- cultural events
- sporting events

## Family

Projects/activities with:

- spouse
- partner
- children
- parents
- relatives

## Administration

- home office supplies
- equipment
- phones
- answering machines
- computers
- internet
- TV
- data backup
- appliances
- entertainment
- filing
- storage
- tools

## Leisure

- books
- music
- videos
- travel
- places to visit
- people to visit
- web browsing
- photography
- sports equipment
- hobbies
- cooking
- recreation

## Financial

- bills
- banks
- investments
- loans
- taxes
- budget
- insurance
- mortgage
- accountants

## Legal

- wills
- trusts
- estate
- legal affairs

## Waiting for

- mail order
- repairs
- reimbursements
- loaned items
- information
- rsvp’s

## Home/household

- real estate
- repairs
- construction
- remodeling
- landlords,
- heating and A/C
- plumbing
- electricity
- roofs
- landscaping,
- driveways
- garages
- walls
- floors
- ceilings
- decor
- furniture,
- utilities
- appliances
- lights and wiring
- kitchen stuff
- laundry,
- places to clear
- cleaning
- organizing
- storage areas

## Health

- doctors
- dentist
- optometrist
- specialists
- checkups
- diet
- food
- exercise

## Personal development

- classes
- seminars
- education
- coaching
- career
- creative
- expressions

## Transportation

- autos
- bikes
- motorcycles
- maintenance
- repair
- commuting
- tickets
- reservations

## Clothes

- professional
- casual
- formal
- sports
- accessories
- luggage,
- repairs
- tailoring

## Pets

- health
- training
- supplies

## Errands

- hardware store
- pharmacy
- department stores
- bank
- cleaners,
- stationers
- gifts
- office supply
- groceries

## Community

- neighbourhood
- neighbours
- service work
- schools
- civic
- involvements
