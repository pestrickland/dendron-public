---
id: yi227g5ceojmz7hxejf0jdb
title: Systems Thinking
desc: ""
updated: 1699703154630
created: 1696453898129
tags:
  - learning
  - systems.thinking
  - systems.engineering
  - complexity
---

## A System

"..a set of related components that work together in a particular environment to perform whatever functions are required to achieve the system's objective"

## Synthesis Vs Analysis

When it comes to systems thinking, the goal is synthesis (as opposed to analysis, which is the dissection of complexity into manageable components).

## Current World View

Systems are everywhere and in everything; it doesn't take much imagination or thought to see the world through a systems lens. One problem I am currently facing is how to manage the relationship between the engineering reductionist view of breaking down a problem/question/task into manageable chunks, which add together to form a whole, and the systems thinking idea of dynamic relationships between all components of a system so that the interactions across the whole can be simultaneously considered.

1. This perspective came about due to my engineering background, and the experience of seeing mostly benefits in this approach. To solve a complex problem it is a good strategy to break it down into individual, small pieces.
2. Many elements of this thinking are reductionist. As soon as you break down the task, and work on the individual elements, you lose some sight of the whole. The interaction and relationships that _may_ be important (but are often non-existent or at the least, negligible), are not as important as the core of each element.
3. Focusing on the _dynamic_ aspects of the systems we work with could be a way to unlock the systems thinking approach. If we think about the emerging solution as an evolution that causes feedback and side effects as it is developed, rather than working out how it all fits together at the end, it may be possible to show some benefits to the rather foreign (at this point) approach.

- [[Try to look at some simple systems in terms of their relationships with one another **first**, and their constituent parts second|projects.2023.11.try-to-look-at-some-simple-systems-in-terms-of-their-relationships-with-one-another-first-and-their-constituent-parts-second]]
