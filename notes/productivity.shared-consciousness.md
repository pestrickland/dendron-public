---
id: 2fvwxispqie73214eforve7
title: Shared Consciousness
desc: ""
updated: 1685285457275
created: 1685285371640
---

According to the McChrystal model[^1]:

> Teams require a regular flow of relevant information to maintain a situational awareness of the environment, what other teams are trying to accomplish, and how they can support those efforts.

[^1]: Livingston, David, _What Kind of Leader Can Lead a Team of Teams? The 6 Principles of Leading Like a Gardene_, McChrystal Group, <https://www.mcchrystalgroup.com/insights/what-kind-of-leader-can-lead-a-team-of-teams-the-6-principles-of-leading-like-a-gardener/>, 12 October 2022.
