---
id: c43qjgcdzxm9b3rfxm727yr
title: How to Run Great Meetings
desc: ''
updated: 1686136088809
created: 1670783061579
tags:
  - productivity
---

This is summarised from a post by Peter Wang on the Async newsletter [^1].

## Avoid if possible

First, avoid them if possible. However, [[they have their place|meetings.good-meetings]]:

- A decision might have a lot of ambiguity
- A decision could be fairly irreversible
- Discussion up to this point has not tended towards a consensus

## Steps to a Good Meeting

1. Set a clear objective
2. Send out background reading and an agenda
3. Decision meetings should be a discussion
4. Take notes live in the meeting
5. Wrap up the meeting and send notes immediately

Check the [[resource.tools.playbooks]] for other strategies.

### Set a Clear Objective

A good meeting serves a useful purpose, and therefore allows you to achieve a desired outcome[^2]. To do this, you need to know what the objective is.

- Do you want a decision?
- Do you want to generate ideas?
- Are you getting status reports?
- Are you communicating something?
- Are you making plans?

Having a good objective should allow you to complete this sentence:

> At the close of the meeting, I want the group to...

### Background Reading

Create a shared document and send it at least a day before the meeting. Encourage everyone to read it and leave feedback in the document before the meeting.

### Discuss

If no one has read the document beforehand, make them read it before starting the discussion. The meeting is for a discussion between people, not a presentation for people to watch silently.

### Use a Live Document for Notes

Share and write decisions and actions as you go so everyone is aligned.

### Recap

With five minutes to go, wrap up the discussion and push for a decision if one is to be made. Send out the notes immediately.

[^1]: <https://async.twist.com/how-to-make-decisions-asynchronously/>
[^2]: <https://www.mindtools.com/afhhhdo/running-effective-meetings>
