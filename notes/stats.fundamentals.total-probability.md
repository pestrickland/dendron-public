---
id: ohiqd5g0cis36jetw6m85bg
title: Law of Total Probability
desc: ''
updated: 1725830784735
created: 1725830732940
---

This relates [[marginal probabilities|stats.marginal-probabilities]] to [[conditional probabilities|stats.fundamentals#conditional-probability]].

$$
\begin{aligned}
P(A) &= \sum_n P(A \cap B_n) \\
&= \sum_n P(A \vert B_n) P(B_n)
\end{aligned}
$$

Another way to look at this is to think about the probabilities of events that are not dependent on one another, such as picking marbles from a selection of bags. Take three bags, A, B and C, each of which can be selected with equal probabilities (i.e. $P(A) = P(B) = P(C) = 1/3$). Each bag contains marbles. Bag A contains 3 red marbles and 2 blue marbles; bag B contains 2 red marbles and 4 blue marbles; bag C contains 5 red marbles.

[[Conditional probability|stats.fundamentals#conditional-probability]] can be used to calculate the probability that you pick a blue marble _given that_ you have chosen bag B at random:

$$
\begin{aligned}
P(blue \vert B) &= \frac{4}{6}
\end{aligned}
$$

What about if you randomly pick a bag, what is the probability of getting a blue marble?

$$
\begin{aligned}
P(\mathrm{blue}) &= P(\mathrm{blue} \cap A) + P(\mathrm{blue} \cap B) + P(\mathrm{blue} \cap C) \\
P(\mathrm{blue}) &= P(\mathrm{blue} \vert A)\cdot P(A) + P(\mathrm{blue} \vert B)\cdot P(B) + P(\mathrm{blue} \vert C)\cdot P(C)
\end{aligned}
$$
