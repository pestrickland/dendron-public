---
id: cp6kedhynwlr1rhxi7m7asi
title: Escape This Podcast
desc: ""
updated: 1731150870029
created: 1731098127080
---

I came to this via Tom Scott's [[podcasts.lateral]].

This podcast has recommended a few others, some of which I keep listening to:

- [[podcasts.blockblunders]]
- [[podcasts.drunkards-walk]]
