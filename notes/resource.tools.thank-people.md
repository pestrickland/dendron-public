---
id: bsf78cfbamoh52qt5xj753w
title: Thank People
desc: ""
updated: 1686135089311
created: 1686135026265
---

Say thank you. Highlight where value has been added. Show people how their direct, individual contributions help the wider goals of the task/project/programme.
