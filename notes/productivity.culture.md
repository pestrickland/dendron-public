---
id: udpqm9h9i6tuoev8kkq6031
title: Culture
desc: ""
updated: 1720798063533
created: 1683384251454
---

## Supporting New Ways of Working

Try to adopt a culture that is **user-centred** and **iterative**.[^1]

[^1]: <https://public.digital/2023/05/02/the-role-of-training-in-transformation>

### User-Centred

> Focused around directly meeting the needs of those using the services.

### Iterative

> Directed towards continuous improvement of services.

## Key Insights

1. Think of training as just the beginning
2. Tailor training to the organisation
3. Provide space for people to connect to the material and each other

### Training is Just the Beginning

When learning something new, a person will progress gradually (see [Dreyfus model of skills acquisition](https://en.wikipedia.org/wiki/Dreyfus_model_of_skill_acquisition)) through the following skill levels:

- Aware
- Novice
- Competent
- Proficient
- Expert

Put training near the beginning of this process so that it contributes to awareness. The experience that follows will be developed from the lived experience of people.

Use training to **align**, **inspire** and **enable** people.

### Tailor the Training to the Organisation

- Challenge people, but don't alienate them
- Bring it back to what people know
- Relate messaging to concepts that are already understood
- Test the training to learn and improve

### Provide Space for People

Use different formats for different types of engagement:

- Broadcast high-level concepts
- Use interactive workshops for people to try things out
- Use individual coaching to allow people to explore the concepts within their own domain and tackle their own challenges

Allow and encourage people to learn from, and help, each other:

- Create opportunities for staff to share stories
- Encourage conversation
- Ensure training groups have diversity in skill sets, knowledge and experience

## It Takes Time

- It's a long journey
- Working out how to connect and inspire people takes time
- Results take even longer to be tangible
- Each stage of the process can still be fruitful
- Stay curious about what you find
