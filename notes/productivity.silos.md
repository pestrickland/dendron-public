---
id: dzvnkuun6dbf2t78uvyn9i5
title: Silos
desc: ""
updated: 1736103938671
created: 1668894437038
tags:
  - research
  - leadership
  - management
  - teams
  - cutlefish
---

[[Develop Silo Note|todo.2022.11.19.develop-silo-note]]

Emily Webber writes some interesting thoughts about siloed working, the focus on specialist disciplines and how this is conflated with people's roles. [^1] In tackling this, the ideas of the following are raised:

- [The wicked problem](https://en.wikipedia.org/wiki/Wicked_problem)
- People are not necessarily T-shaped or Π-shaped people, but rather "broken-comb-shaped"
- [Draw how you make toast](https://www.drawtoast.com/)
- Multidisciplinary vs interdisciplinary vs transdisciplinary teams

## An Alternative View: Frames

An interesting read from Jon Cutler [^2] about the concept of "frames". They look very much like silos but rather than each circle being a person or team, they are a concept or element of the workplace, e.g.:

- Work
- People
- Organisational structure
- Money
- Value
- Products
- Goals and strategies
- Technology
- Competitive ecosystem
- Partner network
- Activities and behaviours
- Knowledge

![Frames](/assets/images/2023-08-21-12-30-25.png)

## NASA vs ELDO

NASA brought much in house for Apollo. It seems that they invented the single source of truth, instantaneous access to information from everyone for everyone (e.g. radio loops), etc.

ELDO (who?) was the European effort. In 1961 the US and Europe were more or less equal. ELDO handed different elements to different companies/different countries. Integration was poor. It failed.

[^1]: <https://emilywebber.co.uk/why-cant-we-all-just-get-along/>
[^2]: <https://cutlefish.substack.com/p/tbm-235-forms-and-shadows>
