---
id: 35oersock4wvola2d6prfyy
title: Language to Help Others Excel
desc: ""
updated: 1699905593230
created: 1699905220948
tags:
  - micropractice
  - skill.giving-feedback
---

Instead of "can I give you some feedback?",
Try "here's my reaction".

Instead of "good job!",
Try "here are three things that really worked for me. What was going through your mind when you did them?"

Instead of "here's what you should do",
Try "here's what I would do".

Instead of "here's where you need to improve",
Try "here's what worked best for me, and here's why".

Instead of "that didn't really work",
Try "when you did _x_, I felt _y_", or "I didn't get that".

Instead of "you need to be more responsive",
Try "when I don't hear from you, I worry that we're not on the same page".

Instead of "you lack strategic thinking",
Try "I'm struggling to understand your plan".

A Cultural Micropractice taken from <https://www.human.pm/>
