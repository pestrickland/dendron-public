---
id: vhes0h7qe6wqtf9ui9ae77p
title: Complexity
desc: ""
updated: 1711904931971
created: 1711903520163
tags:
  - complexity
---

Noted in _[[reading.books.team-of-teams]]_ Chapter 3: From Complicated to Complex

> Being _complex_ is different from being _complicated_.

See also the idea of the [[productivity.cynefin-framework]]. In essence, complicated can just mean that there is a lot going on, but that it can all be understood and comprehended. Complexity is not as simple, since there can be chaotic elements and aspects that cannot be understood.

> Complex systems exhibit **nonlinear** change

The book explores [Edward Lorenz](https://en.wikipedia.org/wiki/Edward_Norton_Lorenz) and the infamous "[butterfly effect](https://en.wikipedia.org/wiki/Butterfly_effect)".

> The term "butterfly effect"... has become synonymous with "leverage"—the idea of a small thing that has a big impact, with the implication that, like a lever, it **can be manipulated to a desired end**. The reality is that small things in a complex system may have no effect or a massive one, and it is virtually **impossible to know which will turn out to be the case**.

Later in the chapter we see that:

> some systems are **essentially complex** (like the human brain, or society), whereas other systems (like a big machine, or a factory) might appear complex because they have a lot of moving parts, but are essentially complicated.

To conclude:

> - The technological changes of recent decades have led to a more **interdependent** and **fast-paced** world. This creates a state of **complexity**.
> - **Complexity** produces a fundamentally different situation from the **complicated** challenges of the past; complicated problems required great effort, but ultimately yielded to prediction. Complexity means that, in spite of our increased abilities to track and measure, the world has become, in many ways, vastly **less predictable**.
> - This **unpredictability** is **fundamentally incompatible with reductionist managerial models based around planning and prediction**. The new environment demands a new approach.
