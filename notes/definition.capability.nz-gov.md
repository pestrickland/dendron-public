---
id: yc0cvrq5z2il4r82ulvcsou
title: New Zealand Government Capability Definition
desc: ""
updated: 1708897716571
created: 1708897401421
tags:
  - capability
---

> "The appropriation combination of competent people, knowledge, money, technology, physical assets, systems and structures necessary to deliver a specified level of performance in pursuit of the organisation’s objectives, now and/or in the future."
