---
id: 86ceha1kd39xccq0syjri2g
title: US DoD Capability Definition
desc: ""
updated: 1708897687816
created: 1708897348546
tags:
  - capability
---

> "The ability to execute a specified course of action. (A capability may or may not be accompanied by an intention.)"
