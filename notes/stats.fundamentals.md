---
id: xmhaf4p1zy32amr90e503qd
title: Fundamentals
desc: ""
updated: 1725830873325
created: 1721305813741
tags:
  - learning
  - statistics
  - bayes
---

## Conditional Probability

![[stats.fundamentals.conditional-probability]]

## Independence

![[stats.fundamentals.independence]]

## Bayes' Theorem

![[stats.fundamentals.bayes]]

## Law of Total Probability

![[stats.fundamentals.total-probability]]
