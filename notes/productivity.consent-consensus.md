---
id: y5vbn9rp37j7ldy9adfn1ix
title: Consent Vs  Consensus
desc: ""
updated: 1687184319095
created: 1687184181268
---

- Consensus: "Agreement or Accord from _everyone_" --> asking "does everyone agree?"
- Consent: "No significant objection from _anyone_" --> asking "does anyone object?"

Source: _FounderCulture_, <https://kb.founderculture.net/public/posts/9dnxaf0s>
