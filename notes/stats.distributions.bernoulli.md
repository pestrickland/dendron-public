---
id: 5udj97gq5eztkftoep19rzc
title: Bernoulli Distribution
desc: ""
updated: 1722803873717
created: 1721481058410
---

This is for two possible outcomes, e.g. success/failure, heads/tails

$$
X \sim \mathrm{B}(p)
$$

If we think about $\mathrm{Pr}(X=1)=p$ as denoting a success, or heads, then we can call tails a failure. Therefore:

$$
\mathrm{Pr}(X=1)=p \\
\mathrm{Pr}(X=0)=1-p
$$

This can also be written as a function:

$$
f(X=x\vert p) = f(x \vert p)
$$

In the case of a Bernoulli distribution:

$$
f(x \vert p) = p^x (1-p)^{1-x} \quad \text{for } x \in \{0,1\}
$$

Using an indicator (or Heaviside) function, this can be written as:

$$

f(x \vert p) = p^x(1-p)^{1-x} 𝟙_{\{x \in \{0,1\}\}}(x)
$$

The indicator function is a step function, and it takes precedence in the order of operations.

### Expected Value

$$
E [X] = \sum_{x}xP(X=x) = (1)p + (0)(1-p) = p
$$

### Variance

$$
\mathrm{Var}(X) = p(1-p)
$$
