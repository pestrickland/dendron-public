---
id: 3j055h0od0xh2ym0ynbja5z
title: Outputs vs Outcomes
desc: ""
updated: 1700300175541
created: 1688908911494
tags:
  - productivity
  - customers
  - outcomes
---

> Output is not outcome[^1]

What matters to customers is what they can do with our output. That is the outcome, along with what they feel and say after we have delivered something to them. Customers get value from a successful outcome. Our business gets value only if we can monetise that in some way.

[^1]: _Tension: Why Product Development Requires Balancing Conflicting Goals_, Jeff Patton & Associates, <https://jpattonassociates.com/tension>
