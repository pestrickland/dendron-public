---
id: kdouxpashlp67tvn475qhoh
title: Official Real Ideal
desc: ""
updated: 1736104027804
created: 1722080454005
tags:
  - cutlefish
---

<https://open.substack.com/pub/cutlefish/p/tbm-303-official-real-and-the-ideal?r=9n2t7&utm_campaign=post&utm_medium=web>

A. The official way things work – the boxes to check, the "formal process"
B. How things work – how work is happening
C. The effort required to translate B into A, or reality into the "official" way
D. An optics-oriented idealisation of the present
E. A well-meaning direction to improve the situation
