---
id: ut1qqclppv0nypz2wfkpqh7
title: Productivity Vault
desc: ""
updated: 1669469304145
created: 1669469304145
---

## Productivity Vault

This vault is where I keep my notes that are related to productivity, business processes, team leadership, etc. It is intended to be the one and only vault that is shared across my home and work lives.
