---
id: 1h751u2ksjo2ty9ty6u4nys
title: Red Work vs Blue Work
desc: ""
updated: 1708799567280
created: 1708799027435
---

### Red Work Vs Blue Work

The colours reflect the _heat_ or energy that is exerted. The colours are also important in breaking down the old concept of blue collar workers vs white collar (manager) workers. The same team can do both blue and red work in different phases. First comes [[blue|reading.books.leadership-is-language.blue-work]] exploring the problem and the solution. Once all that is complete, the [[red work|reading.books.leadership-is-language.red-work]] can begin, and the drive to delivering efficiently can take place.
